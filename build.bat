@echo off

set errorlevel=0

:begin

REM call doctoc.bat 
call gitbook_build.bat 
call gitbook_serve.bat 
TIMEOUT 5

goto begin


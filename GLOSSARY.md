<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Productividad](#productividad)
- [Efectividad](#efectividad)
- [Eficacia](#eficacia)
- [Eficiencia](#eficiencia)
- [Procrastinación](#procrastinaci%C3%B3n)
- [Proyecto](#proyecto)
- [JIT](#jit)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Productividad

La productividad personal es la **cantidad** de trabajo **útil** que un individuo puede sacar adelante en una unidad de tiempo.

* cantidad (medible = cuantificable) de trabajo útil (= efectivo = que produzca un resultado)

F(intensidad de la tarea,concentración,energia...)

## Efectividad

La **efectividad** es el equilibrio entre **eficacia** y **eficiencia**, es decir, se es efectivo si se es eficaz y eficiente.

Enlaces:

* Wikipedia [Efectividad](https://es.wikipedia.org/wiki/Efectividad).

## Eficacia

La **eficacia** es lograr un resultado o efecto (aunque no sea el correcto) y **está orientado al qué**.

## Eficiencia

En cambio, eficiencia es la capacidad de lograr el efecto en cuestión con el mínimo de recursos posibles viable o sea el cómo. Ejemplo: matar una mosca de un cañonazo es eficaz (conseguimos el objetivo) pero poco eficiente (se gastan recursos desmesurados para la meta buscada).

## Procrastinación

[Wikipedia](https://es.wikipedia.org/wiki/Procrastinaci%C3%B3n)

La **procrastinación** (del latín procrastinare: pro, adelante, y crastinus, referente al futuro),2​ postergación o posposición es la acción o hábito de retrasar actividades o situaciones que deben atenderse, sustituyéndolas por otras situaciones más irrelevantes o agradables.

## Proyecto

Planificación que consiste en un conjunto de actividades que se encuentran interrelacionadas y coordinadas. Acciones dirigidas a obtener un resultado común dentro de unos límites de calidad, presupuesto o fechas.

Enlaces:

* [Proyecto - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Proyecto).

## JIT

Método Just In Time ("Justo a Tiempo") es un sistema de organización de la producción para las fábricas, de origen japonés. También conocido como *método Toyota*. La esencia de JIT es que los suministros llegan a la fábrica, o los productos al cliente, "justo a tiempo", eso siendo poco antes de que se usen y solo en las cantidades necesarias.
 
Una compañía que establezca este sistema de producción en sus procesos podría aproximarse al inventario cero (evitando costes innecesarios de producto inmovilizado).





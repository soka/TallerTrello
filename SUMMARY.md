# Summary

* [Introducción al taller](README.md)

### PRODUCTIVIDAD

* [Productividad](doc/chapter-productividad/intro.md)
	* [Primera Ley de Newton sobre el Movimiento ¿Qué tiene que ver la física con la productividad?](/doc/chapter-productividad/01-newton/newton.md)
	* [El principio o ley de Pareto](/doc/chapter-productividad/02-pareto/pareto.md)	
    * [Lo urgente y lo importante: un secreto del ex presidente Eisenhower](/doc/chapter-productividad/03-eisenhower/eisenhower.md)
	* [La ley de Parkinson: Aprovechar el tiempo y los efectivos disponibles](/doc/chapter-productividad/04-parkinson/parkinson.md)
	* [GTD: La metodología de David Allen](/doc/chapter-productividad/05-allen/allen.md)
		* [Presentación MU](/doc/chapter-productividad/recursos/presentacion_mu/presentacion_mu.md).
	* [Técnica Pomodoro - Francesco Cirillo](/doc/chapter-productividad/06-pomodoro/pomodoro.md)
	* [Le Ley de Little](/doc/chapter-productividad/07-little/little.md)
	* [Otras metodologías y buenas prácticas](/doc/chapter-productividad/08-otros/otros.md)

### PROYECTOS
	
* [Gestión de proyectos y metodologías ágiles]()
* [Metodología Kanban](doc/chapter-kanban/01-intro/intro-kanban.md)
	* [Método](doc/chapter-kanban/02-metodo/metodo.md)
	* [Valores](doc/chapter-kanban/03-valores/valores.md)
	* [Prácticas](doc/chapter-kanban/04-practicas/practicas.md)
	* [Roles](doc/chapter-kanban/05-roles/roles.md)
	* [Recursos](doc/chapter-kanban/recursos/recursos.md)

### TRELLO	

* [Trello Web]
	* [Registro inicial](/doc/chapter-trello-web/01-acceso-nuevos-usuarios/trello-registro-usuarios.md)
	* [Nuestro primer tablero](/doc/chapter-trello-web/02-nuestro-primer-tablero/nuestro-primer-tablero.md)
	* [Introducción a Trello](/doc/chapter-trello-web/intro/intro.md)
	* [Mi cuenta de Trello](/doc/chapter-trello-web/mi-cuenta/mi-cuenta.md)
	* [Habilita las notificaciones de escritorio](/doc/chapter-trello-web/notificaciones-escritorio/notificaciones-escritorio.md)
	* [Tableros](/doc/chapter-trello-web/tablero/tablero.md)
		* [Aspectos básicos del tablero](/doc/chapter-trello-web/tablero/aspectos-basicos-tablero/aspectos-basicos-tablero.md)
		* [Atajos de teclado](/doc/chapter-trello-web/tablero/atajos-teclado/atajos-teclado.md)
		* [Búsquedas](/doc/chapter-trello-web/tablero/atajos-teclado/busquedas.md)
		* [Colaboración](/doc/chapter-trello-web/tablero/colaboracion/colaboracion.md)
		* [Crear tarjetas por correo electrónico](/doc/chapter-trello-web/tablero/crear-card-correo/crear-card-correo.md)
	* [Administración de equipo](/doc/chapter-trello-web/admin-equipo/admin-equipo.md)
	* [Tarjetas]
		* [Editar tarjetas](/doc/chapter-trello-web/cards/editar-tarjetas/editar-tarjetas.md)
		* [Markdown](/doc/chapter-trello-web/cards/markdown/markdown.md)		
		* [Referencia a otra tarjeta](/doc/chapter-trello-web/cards/ref-otra-tarjeta/ref-otra-tarjeta.md)
	* [Trucos avanzados](/doc/chapter-trello-web/tips-tricks/tips-tricks.md)	
	* [Extensiones](/doc/chapter-trello-web/extensiones/extensiones.md)
	
### FINAL 
	
* [Recursos para crear el taller](doc/chapter-recursos/chapter-recursos.md)
    * [GitBook](doc/chapter-recursos/gitbook/gitbook.md)
	* [Slack](doc/chapter-recursos/slack/slack.md)


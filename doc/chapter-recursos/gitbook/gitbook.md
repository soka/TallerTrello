# GitBook

La [Web principal](https://soka.gitlab.io/TallerTrello) creada para este taller esta creada con [GitBook](https://www.gitbook.com/)

![](/doc/chapter-recursos/gitbook/img/01.png)

**GitBook** permite crear Webs estáticas de forma muy rápida sin apenas conocimientos técnicos y centrarnos en los contenidos, con un interface muy minimalista y limpia de distracciones.  

El contenido se ordena en forma de estructura de directorios, 

https://medium.com/@gpbl/how-to-use-gitbook-to-publish-docs-for-your-open-source-npm-packages-465dd8d5bfba

## Variables internas:

Muestra nombre del archivo local con ruta:

```
My file is {{ file.path }}
```

```
My name is {{ name }}, nice to meet you
```

## Comandos

```
$ gitbook build --config book.json
$ gitbook serve --config book.json
```

# Plugins 

[https://plugins.gitbook.com/](https://plugins.gitbook.com/).

## book.json

Permite establecer la configuración de generación de GitBook. 

[https://toolchain.gitbook.com/config.html#structure](https://toolchain.gitbook.com/config.html#structure)

# DocToc - Tablas de contenido dinámicas

[DocToc](https://github.com/thlorenz/doctoc) es un páquete NPM para generar tablas de contenido de forma automática dentro de ficheros MarkDown.

Instalación:
```
npm install -g doctoc
```

Una vez instalado usamos los bloques `<!-- START doctoc -->` para iniciar y finalizar con `<!-- END doctoc -->`.  Cuando ejecutemos DocToc sobre el fichero dentro de los bloques creará la tabla de contenidos:

Ejemplo:
```
doctoc README.md
```

# sectionx-fork

Enlace: [https://plugins.gitbook.com/plugin/sectionx-fork](https://plugins.gitbook.com/plugin/sectionx-fork).

This is GitBook plugin created for you to separate the page into sections, and add buttons to allow readers to control the visibility of each section.

Ejemplo:
```
<!--sec data-title="Introduction" data-id="section0" data-show=true ces-->

Insert markdown content here (you should start with h3 if you use heading).

<!--endsec-->
```

Como se ve:

<!--sec data-title="Introduction" data-id="section0" data-show=true ces-->

Insert markdown content here (you should start with h3 if you use heading).

<!--endsec-->

# "smart-nav-collapse"

Enlace: [https://plugins.gitbook.com/plugin/smart-nav-collapse](https://plugins.gitbook.com/plugin/smart-nav-collapse).

This plugin will collapse navigation items with children on Gitbook, including the ones not pointing to a page.






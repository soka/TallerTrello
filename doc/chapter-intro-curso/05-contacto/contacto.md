<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Contacto](#contacto)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Contacto

- **Correo electrónico**: ikernaix@gmail.com.
- **Twitter**: [@ikernaix](https://twitter.com/ikernaix).
- [**Slack**](https://slack.com/): Es una aplicación de comunicación mediante mensajes, el espacio de trabajo se ordena en canales. Slack puede combinar a la perfección con [**Trello**](https://trello.com), tiene aplicación para smartphone ([Slack para Android en Google Play](https://play.google.com/store/apps/details?id=com.Slack)). 

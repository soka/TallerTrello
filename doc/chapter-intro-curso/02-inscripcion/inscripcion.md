<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Inscripción](#inscripci%C3%B3n)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Inscripción

**Es indispensable inscribirse en el formulario Web del siguiente [enlace](https://goo.gl/forms/rJfARqgSEwp3v4cA3)**. Llevar un ordenador portátil cobra bastante sentido especialmente para el segundo día donde se abordarán ejercicios prácticos con [Trello](https://trello.com) de forma colaborativa entre todas las personas asistentes. 

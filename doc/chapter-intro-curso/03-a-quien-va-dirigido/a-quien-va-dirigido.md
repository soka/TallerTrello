<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [A quién va dirigido](#a-qui%C3%A9n-va-dirigido)
  - [Equipos remotos](#equipos-remotos)
  - [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# A quién va dirigido

Creo que especialmente se pueden aprovechar de las prácticas y herramientas colaborativas equipos de trabajo, pequeños y medianos colectivos y asociaciones. Más cuando cada vez es más habitual que la gente trabaje de forma remota o en diferentes momentos **venciendo límites temporales  y espaciales**.

A lo largo del taller **también se expondrán algunas técnicas dirigidas a la productividad personal** y se expondrán ejemplos de como aplicarlas mediante herramientas para reducir el estres y la ansiedad que nos provoca la sensación de no poder abarcar todo lo que queriamos hacer y los días siempre se nos quedan cortos. 

![](images/01.png)



## Equipos remotos

En los **equipos remotos** se trata de favorecer la colaboración a distancia a través de la tecnología. La **comunicación fluida y la transparencia** (los pilares de algunas metodologías que se exponen en el taller) son fundamentales.

## Enlaces

- Trello.com [Trello para equipos remotos](https://trello.com/teams/remote-team-management) 



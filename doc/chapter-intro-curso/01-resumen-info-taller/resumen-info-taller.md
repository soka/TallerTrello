<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Información sobre el taller](#informaci%C3%B3n-sobre-el-taller)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Información sobre el taller

Si quieres informarte mejor sobre cualquier aspecto relacionado con el taller consulta los diferentes medios de [contacto](\doc\chapter-intro-curso\05-contacto\contacto.md)

- El taller es **gratuito**.

- **Fechas impartición**: 27 Miércoles y 28 Jueves de Junio de 19:00 a 21:00 horas.

- **Lugar**: Kultur Etxea de Bilbao La Vieja. C/ Urazurrutia Nº 7 Bilbao ([enlace mapa](https://goo.gl/maps/4zuPh3Egc6m)))

- **Formulario de inscripción**: Para poder asistir es indispensable completar el formulario Web del siguiente [enlace](\doc\chapter-intro-curso\02-inscripcion\inscripcion.md).



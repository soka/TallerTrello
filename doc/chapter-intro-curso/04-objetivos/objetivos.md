<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Objetivos del curso](#objetivos-del-curso)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Objetivos del curso

Al finalizar el curso se busca que adquieras o mejores habilidades para:

* Potenciar equipos de trabajo y mejorar su comunicación.
* Familiarizarse con nuevas metodologías de trabajo ágiles y algunas técnicas de productividad personal.    
* Conocer y saber aplicar las prácticas fundamentales que se proponen aquí de forma general.
* Conocer [**Trello**](https://trello.com) y aplicar las metodologías arriba mencionadas con ejemplos practicos. 

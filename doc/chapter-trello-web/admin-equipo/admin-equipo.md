<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Visibilidad del equipo](#visibilidad-del-equipo)
- [Añadir otros administradores](#a%C3%B1adir-otros-administradores)
- [Configuración del equipo](#configuraci%C3%B3n-del-equipo)
- [Gestión de miembros](#gesti%C3%B3n-de-miembros)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# ¿Qué es un Equipo?

Un equipo Trello es una forma de agrupar a las personas y tableros para hacer que compartir y colaborar sean aún más fáciles.

Puedes hacer que un tablero sea visible solamente para miembros del equipo, y permitirle a los miembros comentar o votar. Además, puedes hacer que a los tableros de tu equipo estén habilitados para la unión de otros miembros de tu equipo. De esta manera, evitas tener que invitar a todas las personas con las que trabajas a cada tablero para que puedan ver en lo que estás trabajando. Para obtener más información acerca de permitir a los miembros del equipo auto-unirse a tableros, por favor ve ["Añadir personas a un Tablero"](https://help.trello.com/customer/portal/articles/920223-adding-people-to-a-board).

# Creando un nuevo equipo

Crear un nuevo equipo haciendo clic en el botón "+" al lado izquierdo del menú de Trello.

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/55f0678fc697911d40651326/file-YqisH32629.png)

También puede crear un nuevo equipo directamente desde la página de inicio haciendo clic en "Crear un equipo" en la sección de equipos a la izquierda.

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/5ade7e752c7d3a5063b4d077/file-EPVcTCUa9V.png)

# Información general del Equipo

Cada Equipo de Trello tiene su propia Vista General de equipo, que muestra los tableros del equipo, miembros y la configuración.

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/55e8a9c490336042eab3e636/file-ll9zkphvwJ.png)

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/55e8abb090336042eab3e63b/file-0KtEGLqoFs.png)

**Edición de los detalles de tu Equipo**: Si eres un administrador de un equipo, puedes editar los detalles de tu equipo haciendo clic en "Editar perfil" en la parte superior. Aquí, puedes cambiar el nombre de tu equipo, el avatar de tu equipo, su sitio web o su descripción, y su "Nombre corto", que afecta a la URL de tu Página de Equipo. Puedes encontrar tu Página de Equipo en https://trello.com/[ID corta de Organización]

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/55e8b50ac69791431cfba753/file-m1IXMKujQ0.png)

**La Pestaña Tableros**: Esta pestaña muestra todos los tableros asociados a tu equipo que son visibles para ti. Si eres el administrador de un Business Class equipo, serás capaz de ver todos los tableros, Incluso si son privados y tú no eres un miembro. Verás todos los tableros que son públicos, visibles al equipo, o en los que tú eres un miembro.

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/55e8b5c690336042eab3e676/file-5kjSZZLIKD.png)

**La Pestaña Miembros**: Esta pestaña muestra a todos los miembros de Trello que están en tu equipo, junto con su nivel de permiso ("Admin" o "Normal"). Si eres un administrador, serás capaz de cambiar el nivel de permiso de los miembros o quitar miembros del equipo.

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/55f1de4fc69791729524c00a/file-BrhScGEBaa.png)

**La pestaña Configuración**: En esta pestaña, puedes cambiar la [visibilidad de tu equipo](https://help.trello.com/article/727-setting-the-visibility-of-a-team-to-public-or-private).

# Visibilidad del equipo

Para acceder a la configuración del equipo podemos usar la página central [https://trello.com/](https://trello.com/) y seleccionar el equipo en la columna lateral izquierda. Cada equipo tiene una "home" en la URL formada por su nombre, por ejemplo [https://trello.com/talleresformacionpublica/home](https://trello.com/talleresformacionpublica/home).
	
Clic en la pestaña "Configuración" en su página de equipo para poder configurar la visibilidad de su equipo.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/team-administration/7589fa28a18eb30e1a2c565c319b02ce/01_es.jpg)

Cuando hablamos de la visibilidad del equipo, nos referimos a el equipo estará indexado, o no, por motores de búsqueda o si será visible para personas que no pertenezcan al equipo. **La visibilidad se puede configurar como pública o privada**. 

# Añadir otros administradores

Los administradores son los únicos miembros del equipo que pueden añadir y eliminar miembros de un equipo, así como cambiar la configuración, de modo que es importante contar con alguien disponible para llevar a cabo los cambios necesarios.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/team-administration/666f8049b3fed970614dd38c666ac408/04_es.png)

Para convertir a otro miembro de tu equipo en administrador: 

* Clic en la pestaña "Miembros" de la página de equipo. 
* Elije a uno de los miembros del equipo.
* Clic en la opción "Normal", junto a su nombre, y cámbiela por "Administrador".

# Configuración del equipo

En la sección de configuración del equipo ([ejemplo talleresformacionpublica](https://trello.com/talleresformacionpublica/account)) editamos el perfil del grupo. Puedes añadir un logotipo de tu equipo haciendo clic en el área de imagen vacía, 

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/team-administration/5ee33b59ce24e9d7e1910a9cd0e2f8c5/05_es.jpg)

Puedes cambiar el nombre del equipo y el nombre corto (el que figura en la URL del equipo), así como añadir el enlace de un sitio web y la descripción del equipo.

# Gestión de miembros

El número de miembros que puede tener en un equipo es ilimitado. Para añadir nuevos miembros al equipo, haz clic en la pestaña "Miembros" y ve a la sección situada a la izquierda de la página de miembros para añadirlos.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/team-administration/2df583af56e3e8ddda36504d67721a13/06_es.png)

Para eliminar a alguien del equipo, clic en la opción "Eliminar" que aparece al lado de su nombre. **Ten en cuenta que esto lo eliminará únicamente del equipo, de modo que también deberá eliminarlo de todos y cada uno de los tableros en los que esté**.

# Asociar un tablero con un equipo

# Enlaces

* [Administración de equipo](https://trello.com/guide/team-administration.html).
* help.trello.com [Equipos Trello](https://help.trello.com/category/700-category).
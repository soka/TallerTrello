<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Abrir una cuenta de usuario en Trello](#abrir-una-cuenta-de-usuario-en-trello)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Abrir una cuenta de usuario en Trello

Para empezar a trabajar con Trello es necesario previamente registrarnos como usuarios de la plataforma. La página principal de [Trello](https://trello.com/) nos permite registrarnos de forma gratuita.

![](/doc/chapter-trello-web/01-acceso-nuevos-usuarios/images/01.png)

Para [crear una cuenta nueva](https://trello.com/signup) solo es necesario proporcionar un nombre, una dirección de correo y una contraseña.

![](/doc/chapter-trello-web/01-acceso-nuevos-usuarios/images/02.png)

Los usuarios con cuenta en Google están de enorabuena, podemos usar la cuenta para registrarnos automaticamente cuando tengamos la sesión de Google iniciada en el navegador.

Cuando creemos la cuenta debemos confirmar nuestra identidad con el correo que nos llegará a la dirección indicada en el formulario.

![](/doc/chapter-trello-web/01-acceso-nuevos-usuarios/images/03.png)

![](/doc/chapter-trello-web/01-acceso-nuevos-usuarios/images/04.png)

Cuando pinchamos sobre el enlace del correo nos sugiere crear nuestro primer tablero: 

![](/doc/chapter-trello-web/01-acceso-nuevos-usuarios/images/05.png)



<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Uso del doble clic para añadir una lista](#uso-del-doble-clic-para-a%C3%B1adir-una-lista)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Uso del doble clic para añadir una lista

Haz doble clic en cualquier espacio en blanco de un tablero para abrir la ventana emergente que te permitirá añadir listas.



# Enlaces

* [Consejos para aprovechar Trello al máximo - Hipertextual](https://hipertextual.com/archivo/2013/05/aprovechar-trello-maximo/).
* [Dangerously Productive: Master Level Trello Tips](https://blog.trello.com/dangerously-productive-master-level-trello-tips)
* [Consejos profesionales de Trello](https://trello.com/guide/pro-tips).
* [Trello: tips & tricks](https://marcjenkins.co.uk/trello-tips-tricks/).
* [The ultimate board of trello tips tricks)](https://trello.com/b/QtjSVKOf/the-ultimate-board-of-trello-tips-tricks).
* [De vuelta en el universo Trello (como sistema de ticketing)](https://ikerlandajuela.wordpress.com/2016/08/06/de-vuelta-en-el-universo-trello-como-sistema-de-ticketing/).
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Boards o tableros](#boards-o-tableros)
- [Cards o tarjetas](#cards-o-tarjetas)
- [Infografía](#infograf%C3%ADa)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Cuando usamos la aplicación Web de Trello los atajos de teclado nos pueden ahorrar mucho tiempo al cabo de una jornada de arduo trabajo, no hace falta memorizar todas, yo tengo varias teclas predilectas como la 'B' para acceder al menú de tableros, una vez habituado a usarla lo hago sin pensar y me resulta más cómodo que buscar el ratón y moverlo hasta el menú.

Si quieres ver una lista de los atajos de teclado disponibles, accede a la página oficial [Teclas de acceso rápido](https://trello.com/shortcuts) o pulsa la tecla "?" de tu teclado.

![](/doc/chapter-trello-web/tablero/atajos-teclado/images/tecla-ayuda-atajos-teclado.png)

# Boards o tableros

Mis predilectas (consultar en la infografía más abajo todas las opciones posibles):

* `B`: **Acceder al menú de tableros**, selecciona el que quieres visualizar, filtrando tableros con las primeras letras del nombre o navegando por ellos con las teclas de dirección, pulsa enter para entrar dentro.
* Navegar por las listas y tarjetas con las teclas de dirección.
* `ENTER`: Abrir una tarjeta.
* `/`: Búsqueda de terminos **¡De las mejores!**.
* `N`: Nueva tarjeta.
* `E`: Editar una tarjeta.
* `F`: Despliega menú lateral derecho para filtras tarjetas por etiquetas, miembros.. (`X` quitar filtros).
* `Q`: Ver las tarjetas asignadas a ti, el mejor remedio cuando se te va de las manos y no sabes cuales sn tus tareas.

# Cards o tarjetas

Estas las uso menos la verdad:

* `@`: **Esta es indispensable aprender**, permite escribir a un miembro del tablero (`@nombre`), a todas las personas asignadas a la tarjeta (`@card`) o includo todos los miembros del tablero (`@board`).
* `L`: Menú etiquetas, también funciona desde la vista de tablero.
* `C`: Archivar una tarjeta, una especie de borrado lógico.


# Infografía 

![](/doc/chapter-trello-web/tablero/atajos-teclado/images/keyboard-shortcuts-fixedspacing.jpg)

# Enlaces

* blog.trello.com [Trello Keyboard Shortcuts: An Infographic Cheat Sheet](https://blog.trello.com/trello-keyboard-shortcuts-infographic).
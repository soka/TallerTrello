<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Haz visible el tablero para el resto del equipo](#haz-visible-el-tablero-para-el-resto-del-equipo)
- [Añadir miembros al tablero](#a%C3%B1adir-miembros-al-tablero)
- [Añadir miembros a las tarjetas](#a%C3%B1adir-miembros-a-las-tarjetas)
- [Añadir un miembro vs. Seguir](#a%C3%B1adir-un-miembro-vs-seguir)
- [Comentarios, menciones y más](#comentarios-menciones-y-m%C3%A1s)
- [Marcar como hecha](#marcar-como-hecha)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Haz visible el tablero para el resto del equipo

Cuando colabores con un [equipo](http://help.trello.com/article/927-what-are-teams) en Trello, asegúrate en primer lugar de que sus tableros están asociados al equipo y, a continuación, deja que la colaboración fluya **haciendo que tus tableros sean "visibles para el equipo"**. Que un tablero sea visible para el equipo significa que todos los integrantes del equipo pueden ver el tablero y unirse sin necesidad de molestar a un miembro que ya forme parte del tablero para poder acceder.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/e01e34b7a217860e7d52aff61b745564/collaborate-setup-permissions.png)

**Modificar el acceso a la visibilidad del tablero constituye una manera sencilla de añadir transparencia y de terminar con los silos de información.**

# Añadir miembros al tablero

El número de miembros que puede añadir a un tablero es ilimitado. Puedes añadir a todo el equipo o únicamente a aquellos que sean imprescindibles para la tarea en cuestión. Cada vez que añadas a un miembro a un tablero, este recibirá una notificación en Trello y otra por correo electrónico.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/45ab8937ec397dbae857a2fdab1e3518/collaborate-addmembers.png)

Pasos para añadir miembros a un tablero:

* Menú de la barra lateral, situado en la parte derecha del tablero, y verá una sección denominada "Miembros", próxima a la parte superior.
* Clic en el botón "Añadir miembros" e introduzca sus nombres (si están en Trello) o sus direcciones de correo electrónico (si son nuevos en Trello).
* También se puede compartir el enlace de invitación situado en la parte inferior de la ventana "Añadir miembros" para permitir que otras personas se unan al tablero.

# Añadir miembros a las tarjetas

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/2656d589a48daf96d5f806fa09f520f8/collaborate-addtocard.gif)

Para que haya transparencia, añade los miembros del tablero a las tarjetas de las que son responsables:

* Arrastrar el avatar de un miembro desde la barra lateral hasta una tarjeta.
* Clic en el botón "Miembros" de una tarjeta abierta y selecciona a los miembros que le gustaría añadir.
* Unirte tu mismo a las tarjetas con facilidad colocando el cursor del ratón encima de una tarjeta y presionando la barra espaciadora.
* Añade a tantos miembros como sea necesario a una tarjeta para que todos sepan quién hace qué.

Cuando se añada una persona a una tarjeta, esta recibirá una notificación y podrá empezar a trabajar con un simple clic. Los miembros de una tarjeta se suscriben automáticamente a ella, lo que significa que recibirán notificaciones adicionales cada vez que se realice algún cambio en la tarjeta o en las fechas de vencimiento, o si se mueve la tarjeta. 

# Añadir un miembro vs. Seguir

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/d0453a4d5884675b5ff23605e1419aaa/watchlist.jpg)

Se pueden seguir tarjetas, listas individuales o a todo un tablero de Trello.

Ejemplos:

* Los gestores de un equipo o un proyecto que deseen recibir notificaciones cuando se completen las tareas pueden seguir la lista “Hecho” del tablero.

# Comentarios, menciones y más

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/249e5a3a2ba04ac59d25744905fc69ac/collaborate-mention.png)

A menudo, la colaboración genera grandes debates acerca del trabajo que se está realizando.

Consejos:

* Menciona a una persona en un comentario con una @ para que reciba una notificación que la llevará directamente a la tarea en cuestión.
* Menciona a todos los miembros de una tarjeta o de un tablero con @card y @board, respectivamente.

# Marcar como hecha

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/50c527e4883a24336efd9d86b16a2e4f/collaborate-markdone.gif)

Estar al corriente del trabajo que se está realizando sin necesidad de preguntar supone un gran ahorro de tiempo para todos los miembros implicados. 

* Con un solo clic, [marca las tarjetas que tengan fechas de vencimiento como finalizadas](http://blog.trello.com/mark-a-trello-card-as-done?utm_source=guide&utm_medium=collaborate).
* Añade una etiqueta "Hecho" a una tarjeta.
* Simplemente, arrastra la tarjeta hacia la lista titulada "Hecho". Podemos utilizar más de una única lista de tarjetas finalizadas, una vez por semana o una vez al mes.




# Enlaces

* [Colaborar: Los mejores procedimientos para garantizar una colaboración de calidad 
con su equipo.](https://trello.com/guide/collaborate.html).
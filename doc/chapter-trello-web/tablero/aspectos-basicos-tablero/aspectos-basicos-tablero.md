<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Trello: Elementos básicos](#trello-elementos-b%C3%A1sicos)
  - [Tableros: Tus proyectos](#tableros-tus-proyectos)
  - [Listas: Agrupan las tarjetas](#listas-agrupan-las-tarjetas)
  - [Tarjetas: La unidad básica de información](#tarjetas-la-unidad-b%C3%A1sica-de-informaci%C3%B3n)
  - [Menú: Configuración](#men%C3%BA-configuraci%C3%B3n)
- [Listas](#listas)
- [Tarjetas](#tarjetas)
- [Menú](#men%C3%BA)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Trello: Elementos básicos

## Tableros: Tus proyectos

**Un tablero representa un proyecto o un lugar en el que mantener el seguimiento de la información**. Poco importa si está preparando el lanzamiento de un nuevo sitio web, creando un manual del empleado o planeando sus vacaciones. Los tableros de Trello son la herramienta perfecta para organizar sus tareas, que figurarán como tarjetas en listas, y colaborar con sus compañeros de trabajo, familia y amigos.

## Listas: Agrupan las tarjetas

Las listas **mantienen las tarjetas organizadas en sus diversas fases de progreso**. Pueden utilizarse para crear un flujo de trabajo en el que las tarjetas se muevan de una lista a otra desde el inicio hasta el final o, simplemente, actuar como una herramienta en la que mantener el seguimiento de ideas e información. Puede añadir una cantidad ilimitada de listas a un tablero.

## Tarjetas: La unidad básica de información

**Las tarjetas son las unidades fundamentales de un tablero y se utilizan para representar tareas e ideas**. Una tarjeta puede ser algo que debe hacerse, como la redacción de una publicación para un blog, o algo que debe recordarse, como las políticas de vacaciones de una empresa. Arrastre y suelte las tarjetas de una lista a otra para mostrar el progreso. Puede añadir una cantidad ilimitada de tarjetas a un tablero.

## Menú: Configuración 

El menú está situado en el lado derecho de su tablero de Trello y es el centro de control de misiones de su tablero. El menú es la herramienta desde la que gestiona a los miembros, controla la configuración, filtra las tarjetas y habilita los Power-Ups. Además, puede observar toda la actividad que se ha realizado en un tablero en la lista de actividades del menú. Tómese unos minutos para comprobar todo lo que el menú puede ofrecerle.

# Listas

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/2369d89b8369e2e6b147f6ac408eef8d/boardbasics-basiclists_es.png)

Para crear las listas no hay ningún criterio especial, Trello es muy flexible y deja estos aspectos abiertos para nombrar las listas como queramos y crear tantas como sean necesarias. Una práctica común para dar los primeros pasos es crear como mínimo tres listas para controlar el progeso de casi cualquier tarea  con los siguientes estados: "Por hacer", "En proceso" y "Hecho" (o "Inbox" o "Backlog", "Doing" y "Done"). Las nuevas tareas van entrando en "Por hacer" en la lista de la izquierda y se van moviendo por el resto de las listas hacia la derecha a medida que una tarea progresa. 

**Lo más importante es establecer un proceso a través de la creación de un flujo de trabajo que se adapte bien a su equipo** (ver en enlaces tableros propuestos en Trello).

# Tarjetas

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/c7779c4ed4c9f1f7ae00b2cf131dec2b/boardbasics-cards_es.jpg)

Una tarjeta comienza con su título claro. Una tarjeta puede representar cualquier cosa, una tarea o un medio para compartir información por ejemplo. Los títulos de las tarjetas deberían ser claros y concisos para facilitar su búsqueda posterior. Haciendo clic sobre una tarjeta puede acceder y editar su contenido.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/565b9f664f9cdc4423d3fafdad7a90bd/boardbasics-cardback_es.jpg)

Elementos:

* **Descripciones de tarjetas**: Para añadir detalles a su tarjeta, haga clic en "Editar la descripción" en la parte superior del reverso de la tarjeta. En el campo de descripción puede añadir información más específica sobre su tarjeta, enlaces a sitios web o instrucciones paso a paso. Incluso puede aplicar formato al texto con [Markdown](http://help.trello.com/article/821-using-markdown-in-trello).

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/9ead65b381c1efadba7bb0521432ae64/boardbasics-comments_es.jpg)

* **Comentarios y actividad**: Se pueden añadir comentarios a las tarjetas al comunicarse o colaborar con los miembros del equipo para, por ejemplo, transmitir opiniones o actualizaciones. Mencione a un miembro de su tablero o equipo en un comentario con una @ y este recibirá una notificación en Trello. La fuente de actividades muestra el historial de comentarios y acciones que se han realizado en una tarjeta y crea una línea temporal de eventos a medida que se avanza, como cuando se mueve una tarjeta a una lista diferente o cuando alguien completa un elemento de la checklist.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/1cfaf77b3b22cfa6664391e7913c5579/boardbasics-addsection_es.jpg)

* **Añadir**: La sección “Añadir” proporciona más herramientas para el reverso de una tarjeta.
	* **Miembros del tablero a las tarjetas** para asignarles tareas. Los miembros añadidos a las tarjetas recibirán las notificaciones de las acciones que se realicen en esa tarjeta.
	* **Etiquetas a categorías** y etiquetas que se puedan nombrar y filtrar.
	* **Checklists** a la tarjetas que requieran subtareas, también se puede utilizar para enlazar documentos, tarjetas relacionadas o enlaces con información de interés. Incluso se puede [copiar checklists](http://help.trello.com/article/737-adding-checklists-to-cards) de otras tarjetas en el tablero.
	* **Fecha de vencimiento** a las tarjetas que deban cumplir un plazo para que los miembros de la tarjeta reciban una notificación 24 horas antes de que termine el plazo. Una vez que se terminen las tareas, se pueden marcar las fechas de vencimiento como finalizadas.
	* **Ficheros adjuntos**. Si es algo que queremos mantener deberia guardarse mejor en un repositorio (DropBox, Google Drive o OneDrive de MS). Las imágenes en formato .jpeg, .gif o .png se convertirán en una **portada de la tarjeta** y se mostrarán también en la parte delantera de la tarjeta.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/a83ce9d113bc968e41e3987f5aafc250/boardbasics-actionssection_es.jpg)	

* **Acciones**: La sección "Acciones" cuenta con algunas funcionalidades prácticas que vale la pena conocer.
	* **Mover** las tarjetas a otras listas y tableros. 
	* **Copiar** las tarjetas en otras listas y tableros conservando las checklists, los miembros y los adjuntos. Esta capacidad resulta muy útil a la hora de crear un proceso repetitivo, ya que evita tener que empezar de cero una y otra vez.
	* **Suscríbirse** a las tarjetas cuando no necesites añadirte a ellas, pero aun así quieras estar al tanto y recibir las notificaciones de las acciones que se llevan a cabo en esa tarjeta.
	* **Archivar** las tarjetas cuando haya terminado con ellas. Es un borrado "lógico", las tarjetas archivadas se pueden recuperar en caso de que las vuelvas a necesitar.

**Cada una de las tarjetas de Trello cuenta con una dirección de correo electrónico única para cada miembro del tablero***. Esto está muy bien para extraer la información del correo electrónico e introducirla en las tarjetas modificables de Trello. Haz clic en "Compartir y más" para conseguir la dirección de correo electrónico asociada de forma exclusiva a una tarjeta.

# Menú

El menú está situado en el lado derecho de su tablero de Trello y es el centro de control del tablero. El menú es la herramienta desde la que gestiona a los miembros, controla la configuración y habilita los Power-Ups. 

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/33692f8ec90f5e30f1b8f92eefcaa69d/boardbasics-addmembers_es.jpg)

Algunos de los elementos más interesantes del menú:

* **Miembros del tablero**.
	* Clic en **"Añadir miembros"** para añadir personas a un tablero e invítalas utilizando su correo electrónico o su nombre de usuario de Trello. Los miembros del equipo aparecen automáticamente. Puedes invitar a varias personas a un tablero con facilidad enviándoles el enlace especial que aparece en la parte inferior de la ventana "Añadir miembros".
	* **Eliminar miembros de un tablero**. En la sección de miembros, clic en sus avatares y seleccionar la opción "Eliminar del tablero". Por defecto, cualquier miembro del tablero puede añadir y eliminar a otros miembros para que la colaboración resulte más sencilla, aunque los administradores pueden modificar este permiso en la [configuración del tablero](http://help.trello.com/article/791-changing-permissions-on-a-board).
	* Gestiona los permisos de los miembros del tablero haciendo clic en un avatar y seleccionando la opción "Cambiar permisos".  Los administradores del tablero son los únicos que pueden cambiar los permisos de los miembros del tablero.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/259d2cab7af1ab5710185046446f8f77/boardbasics-backgrounds.jpg)
	
* **Cambiar fondo**: Los tableros de Trello vienen con un fondo azul de Trello por defecto. puedes elegir entre ocho colores de fondos adicionales o imágenes. Yo por ejemplo uso colores diferentes para cada equipo.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/6a3b433d7d4995ec9a3c05665404aad4/boardbasics-filter_es.jpg)

* **Filtrar tarjetas**: Para encontrar la información con rapidez, filtra las tarjetas de un tablero **por título, etiqueta, miembro y fecha de vencimiento.** Para filtrar con rapidez todas las tarjetas que tiene asignadas en un tablero, usa el atajo de teclado teclado "Q".

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/3a482ba52cadc08d5c68bfd760ceae1f/boardbasics-power-upscroll.gif)

* **Power-Ups**: Permite aumentar las funcionalidades. Cada tablero viene con un Power-Up gratuito (Calendario y Antigüedad de las tarjetas).

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/cd4bd9260be3b09002b1cf74c5f9b5da/boardbasics-stickers_es.jpg)

* **Pegatinas**.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/0330931ee435805bbda1272c1e7e67f0/boardbasics-more_es.jpg)

* **Más**.
	* Permisos, la configuración y las invitaciones del tablero.
	* Acceso a las tarjetas y listas archivadas, y vuelve a enviarlas al tablero si necesita integrarlas de nuevo en el flujo de trabajo.
	* Copias de un tablero, pude ser útil para elaborar plantillas para procesos repetitivos.
	* ¿Ha terminado con un tablero? Ciérralo para eliminarlo de tu página de tableros, ten en cuenta que esta acción les cerrará el tablero a todos los demás también. siempre puedes abrirlo otra vez.

![](https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/daaf2248c08bf25ee1330589d122d8a3/boardbasics-activity_es.jpg)
	
* **Actividad**: Vista cronológica de todo lo que se ha realizado en un tablero en la fuente de actividades. La sincronización es constante.

# Enlaces 

* [Aspectos básicos del tablero: Tableros, listas, tarjetas y mucho más.](https://trello.com/guide/board-basics.html).
* [Inspiraciones para Trello: Inspírese con estas plantillas de tablero de Trello para organizar todos los proyectos de su vida.](https://trello.com/inspiration).
	* [Productividad](https://trello.com/inspiration/productivity): [Búsqueda empleo](https://trello.com/b/zGRPWTSQ/job-hunt), [Ejemplo To-Do`s personal semanal](https://trello.com/b/hSfWe8Q3/kevans-weekly-to-dos).  
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Crear tarjetas por correo electrónico](#crear-tarjetas-por-correo-electr%C3%B3nico)
- [Consejos de formato](#consejos-de-formato)
- [Enviar comentarios por correo electrónico a una tarjeta](#enviar-comentarios-por-correo-electr%C3%B3nico-a-una-tarjeta)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Crear tarjetas por correo electrónico

Las tarjetas se pueden crear a partir de un correo electrónico. 

Para obtener la dirección de correo electrónico de una tarjeta en el el menú en la barra lateral derecha y selecciona "Más", y luego "Configuración de correo electrónico a tablero".

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/5490896ee4b07d03cb250dcd/file-Q8kXfzeZsV.png)

Cada tablero de Trello tiene una dirección de correo electrónico única.

# Consejos de formato

Cuando envíes por correo electrónico una tarjeta a Trello, puedes añadir inmediatamente archivos adjuntos, etiquetas, y miembros a la tarjeta. Además, también puedes añadir el título y la descripción.

* El asunto del correo electrónico se convertirá entonces en el título de la tarjeta.
* El cuerpo del mensaje electrónico se convertirá en la descripción de la tarjeta.
* Los archivos adjuntos que tenga el correo electrónico también se añadirán a la tarjeta.
* Etiquetas: En el tema, añadir #labelname, #labelcolor, o #labelnumber
	* Si tu etiqueta consta de dos o más palabras, en la línea de asunto o bien unirse a las palabras o el uso de relieve entre las palabras. Por ejemplo, si la etiqueta se denomina "hacer" en la línea de asunto del correo electrónico o bien introducir #ToDo o #To_Do que la etiqueta se muestra correctamente en su tarjeta.
	* Si tiene varias etiquetas en una tabla con el mismo color o del mismo nombre, enviar por correo electrónico un tablero con #color o #nombre sólo añadir la primera etiqueta con ese color o el nombre de la tarjeta.
* Miembros: En el tema, añadir @nombredeusuario. Los miembros también pueden ser añadidos poniéndoles @nombredeusuario en el cuerpo del correo electrónico en su misma línea. Si envías un correo electrónico a Trello e incluyes direcciones de correo electrónico de otros usuarios Trello como "a" o "CC", Trello también los añadirá como miembros de la tarjeta.

Existe un límite que el correo no podrá superar (el cual es de 10 MB).

# Enviar comentarios por correo electrónico a una tarjeta

Al igual que sucede con los tableros, todas las tarjetas tienen su propio correo electrónico. Por lo tanto, si envías un comentario a la dirección de la tarjeta, tu correo electrónico aparecerá como un comentario. Si quieres saber cuál es la dirección de correo electrónico de una tarjeta, accede a la opción "Compartir y otras acciones..." que encontrarás en la parte posterior de la tarjeta, en la esquina inferior derecha. 

![](https://d33v4339jhl8k0.cloudfront.net/docs/assets/545804d8e4b09c5ca72525ce/images/5519b8f8e4b0221aadf241d3/file-7e1eAIM88C.png)

# Enlaces

* [Crear tarjetas por correo electrónico](https://help.trello.com/article/809-creating-cards-by-email).


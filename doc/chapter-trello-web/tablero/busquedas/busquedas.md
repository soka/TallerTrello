<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Búsqueda global](#b%C3%BAsqueda-global)
- [Filtro de listas de tablero](#filtro-de-listas-de-tablero)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Búsqueda global

* [Tarjetas asignadas a un usuario](https://trello.com/c/o8h2Hmku/15-b%C3%BAsquedas-predefinidas-tarjetas-asignadas-a-usuario-trello-b%C3%BAsquedas).
* [Tarjetas abiertas](https://trello.com/c/Tp11BNCM/18-b%C3%BAsqueda-avanzada-tarjetas-abiertas-trello-tarjetas-b%C3%BAsquedas).

# Filtro de listas de tablero 


# Enlaces

* [8 Consejos para buscar rápidamente y ordenar trello tarjetas y listas](http://gocrazyx.info/productividad/30273-8-consejos-para-buscar-r%C3%A1pidamente-y-ordenar.html).
* [Buscar tarjetas (en todos los tableros)](https://help.trello.com/article/808-searching-for-cards-all-boards).
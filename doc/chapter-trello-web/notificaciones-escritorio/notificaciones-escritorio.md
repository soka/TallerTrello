<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Chrome](#chrome)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Chrome

Trello puede ser una excelente herramienta para aumentar nuestra productividad, pero la realidad es que si no le prestamos atención, pasa sin pena ni gloria por nuestras vidas. Por eso, una buena forma de estar siempre encima de lo que está sucediendo es a través de las notificaciones de escritorio. Esta funcionalidad está disponible para Google Chrome, y para Safari 6, pero si usamos estos navegadores específicos, podremos estar más informados de los vaivenes de un proyecto.

Para activarlo en Chrome es necesario hacer los siguientes pasos: 

1. Abre Chrome en tu ordenador.
2. En la parte superior derecha, haz clic en Más Más a continuación Configuración
3. En la parte inferior, haz clic en Configuración avanzada.
4. En la sección "Privacidad y seguridad", haz clic en Configuración de contenido.
5. Haz clic en Notificaciones ([enlace rápido](chrome://settings/content/notifications)). 
6. Selecciona si quieres bloquear las notificaciones o si quieres permitirlas:
	* Bloquear todas: desactiva Preguntar antes de enviar.
	* Bloquear un sitio web: junto a "Bloquear", haz clic en Añadir. Introduce un sitio web y haz clic en Añadir.
	* Permitir un sitio web: junto a "Permitir", haz clic en Añadir. Introduce un sitio web y haz clic en Añadir.

# Enlaces 

* Tarjeta [Trello: Activar notificaciones de escritorio](https://trello.com/c/HNeIiII5/2-trello-activar-notificaciones-de-escritorio-trello-navegador-chrome).
* Ayuda de Google Chrome [Cómo activar o desactivar las notificaciones](https://support.google.com/chrome/answer/3220216?hl=es-419).
* [Algunas ideas y consejos para sacarle todo el jugo a Trello](https://hipertextual.com/archivo/2013/05/aprovechar-trello-maximo/).
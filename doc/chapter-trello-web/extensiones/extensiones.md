<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Websites for Trello](#websites-for-trello)
- [Elegantt](#elegantt)
- [Crear una tarjeta de Trello desde cualquier página web](#crear-una-tarjeta-de-trello-desde-cualquier-p%C3%A1gina-web)
- [Trello para Chrome](#trello-para-chrome)
- [Gmail to Trello](#gmail-to-trello)
- [Show Card Numbers](#show-card-numbers)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Websites for Trello

Vale, quizás este complemento no sea esencial para el uso de Trello, pero me ha llamado tanto la atención que no he podido resistirme a incluirlo. Con [Websites for Trello](http://websitesfortrello.com/) puedes crear, gestionar y mantener un sitio web completo, basado totalmente en Trello.

# Elegantt

Otra extensión de Chrome que complementa Trello es [Elegantt](https://elegantt.com/), que sirve para un propósito muy concreto: añadir al tablero de Trello un diagrama de Gantt generado automáticamente a partir de la información contenida en las tarjetas. De esta forma tienes una visión muy clara y muy gráfica de cómo se expanden las diferentes fases de tu proyecto a lo largo de un cierto período de tiempo.


# Crear una tarjeta de Trello desde cualquier página web

1. Arrastre este enlace hasta sus marcadores:
[Enviar a Trello](javascript:(function(win%2Cname%2Cdesc)%7Bwin.open('https%3A%2F%2Ftrello.com%2Fadd-card'%2B'%3Fsource%3D'%2Bwin.location.host%2B'%26mode%3Dpopup'%2B'%26url%3D'%2BencodeURIComponent(win.location.href)%2B(name%3F'%26name%3D'%2BencodeURIComponent(name)%3A'')%2B(desc%3F'%26desc%3D'%2BencodeURIComponent(desc)%3A'')%2C'add-trello-card'%2C'width%3D500%2Cheight%3D600%2Cleft%3D'%2B(win.screenX%2B(win.outerWidth-500)%2F2)%2B'%2Ctop%3D'%2B(win.screenY%2B(win.outerHeight-740)%2F2))%7D)(window%2Cdocument.title%2CgetSelection%3FgetSelection().toString()%3A''))
2. Visite la página que desea enviar a Trello
3. Cuando se encuentre en una página que desea convertir en una tarjeta, haga clic en su marcador.

[https://trello.com/add-card](https://trello.com/add-card)

# Trello para Chrome

Hay varias extensiones de Chrome que son muy útiles para Trello - entre ellas, la [extensión oficial](https://chrome.google.com/webstore/detail/trello/dmdidbedhnbabookbkpkgomahnocimke) que los propios desarrolladores de Trello han creado para el navegador web de Google. Con esta extensión puedes realizar diversas operaciones propias de Trello directamente en el navegador, sin tener que visitar específicamente el tablero de Trello correspondiente o buscar una tarjeta concreta.

Podrás, por ejemplo, buscar en Trello tal y como buscas desde la barra de direcciones de Chrome, y crear nuevas tarjetas con sólo hacer un clic en el botón de la extensión, estés en la página web que estés.


# Gmail to Trello

[Gmail to Trello](https://chrome.google.com/webstore/detail/gmail-to-trello/oceoildfbiaeclndnjknjpfaoofeekgl/), una extensión de Chrome que añade un nuevo botón a la interfaz de Gmail con el que puedes convertir cualquier mensaje de correo electrónico en una nueva tarjeta de Trello de forma automática.

# Show Card Numbers

Show card numbers on the front of the card [http://goo.gl/yKfjV](http://goo.gl/yKfjV)

* [Show Card Numbers](https://trello.com/c/PkIrgKzd/36-show-card-numbers).

# Enlaces

* [9 herramientas esenciales para complementar Trello](https://www.genbeta.com/herramientas/9-herramientas-esenciales-para-complementar-trello).
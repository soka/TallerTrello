<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Creando nuestro primer tablero](#creando-nuestro-primer-tablero)
  - [Controles y elementos principales del tablero](#controles-y-elementos-principales-del-tablero)
- [Área de trabajo](#%C3%A1rea-de-trabajo)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/board_basics_of_tello.png)

# Creando nuestro primer tablero

Si seguimos la sugerencia de crear un nuevo tablero cuando creamos por primera vez nuestra cuenta de usuario, damos un nombre de nuestra elección a nuestro primer tablero de pruebas, Trello se encarga incluso de crear algunas listas iniciales: Lista de tareas, en proceso y hecho.

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/01.png)

## Controles y elementos principales del tablero

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/02.png)

En la esquina superior izquierda un botón con la leyenda "Tableros", al clicar sobre el se despliegan todos los tableros sobre los que trabajamos, pueden ser nuestros o de equipos de trabajo a los que pertenecemos, podremos salta de forma rápida entre tableros usando este control. 

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/03.png)

Adyacente al menú "Tableros" encontramos un buscador, permite buscar tarjetas que contengan terminos o palabras que buscamos entre todos los tableros que manejamos.

En la esquina superior derecha tenemos tres controles más.

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/04.png)

El primero por la izquierda permite crear:

* Crear nuevo tablero.
* Crear nuevo equipo.
* La tercera opción de crear no me molesto en mencionarla ya que es para usuario de pago.

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/05.png)

El siguiente control es informativo 

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/06.png)

El siguiente botón de gran utilidad, permite seguir de forma cronologica las notificiaciones de todos los tableros, las notificaciones registran diferentes actividades como: Comentarios en una tarjeta, movimientos de tarjetas, cuando se creen o archiven tarjetas y un largo listado de eventos.

Más adelante profundizaremos más en las notificaciones ya que se merecen su propio apartado.

![](/doc/chapter-trello-web/02-nuestro-primer-tablero/images/07.png)

El último control en la esquina superior derecha es el que permite realizar ajustes de la configuración de nuestra cuenta, ver las tarjetas que tenemos asignadas o la actividad de esta cuenta.


# Área de trabajo

En el área de trabajo podemos cambiar el nombre del tablero, si marcamos el tablero como favorito saldrá entre los primeros en la lista de tableros.

Los tableros **pueden ser personales o pertenecer a un equipo** de trabajo, más adelante se mencionará como funcionan los equipos. 

Es importante saber **como se comparten o visibilizan los tableros**, pueden ser:

* Privado: Solo las personas añadidas al tablero pueden verlo y editarlo.
* Equipo: Pueden verlo los miembros del equipo.
* Público: Lo puede ver  


# Enlaces

* Trello help [Recibir notificaciones de Trello](https://help.trello.com/article/793-receiving-trello-notifications).
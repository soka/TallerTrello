<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Da formato al texto de tus tarjetas con Markdown](#da-formato-al-texto-de-tus-tarjetas-con-markdown)
  - [Sintaxis para descripciones de tarjetas, comentarios, listas y tu biografía](#sintaxis-para-descripciones-de-tarjetas-comentarios-listas-y-tu-biograf%C3%ADa)
    - [Tachado](#tachado)
    - [Cursiva](#cursiva)
    - [Negrita](#negrita)
    - [Enlaces](#enlaces)
  - [Sintaxis solo para descripciones de tarjetas y comentarios](#sintaxis-solo-para-descripciones-de-tarjetas-y-comentarios)
    - [Citas](#citas)
    - [Línea horizontal](#l%C3%ADnea-horizontal)
    - [Encabezados](#encabezados)
    - [Listas](#listas)
  - [Sintaxis que sólo funciona en descripciones de tarjetas](#sintaxis-que-s%C3%B3lo-funciona-en-descripciones-de-tarjetas)
    - [Imágenes](#im%C3%A1genes)
- [Enlaces](#enlaces-1)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Da formato al texto de tus tarjetas con Markdown

## Sintaxis para descripciones de tarjetas, comentarios, listas y tu biografía

![](/doc/chapter-trello-web/cards/markdown/images/markdown-card.png)

![](/doc/chapter-trello-web/cards/markdown/images/markdown-card-res.png)


### Tachado 

Este texto es ~~tachado~~.

```
Este texto es ~~tachado~~
```

### Cursiva

Este texto es _italic_

```
Este texto es _italic_
```

### Negrita

Esto es texto en **negrita**.

```
Esto es texto en **negrita**.
```


### Enlaces

El texto del enlace rodeado de `[]` y el enlace con `()` por ejemplo:

```
[Búscador DuckDuckGo](https://duckduckgo.com/)
```

Se mostraría así [Búscador DuckDuckGo](https://duckduckgo.com/) (efectivamente esta Web está escrita con MarkDown).


## Sintaxis solo para descripciones de tarjetas y comentarios

### Citas 

> “El fracaso es, a veces, más fructífero que el éxito.”  Henry Ford

```
> “El fracaso es, a veces, más fructífero que el éxito.”  Henry Ford
```

### Línea horizontal

Una línea formada por al menos tres guiones creará una línea horizontal a lo largo de toda la descripción o comentario. 

--- 

```
---
```

### Encabezados

Podemos crear encabezados de diferentes niveles.

```
# Heading1
## Heading2
### Heading3
```

### Listas

Podemos crear listas ordenadas o "desordenadas".

* Elemento 1
	* Elemento 1.1
* Elemento 2
* Elemento 3

```
* Elemento 1
	* Elemento 1.1
* Elemento 2
* Elemento 3
```

Lista numerada:

1. Elemento 1
2. Elemento 2
3. Elemento 3

```
1. Elemento 1
2. Elemento 2
3. Elemento 3
```

## Sintaxis que sólo funciona en descripciones de tarjetas

### Imágenes 

Añade una imagen poniendo el texto del enlace entre corchetes y la URL o la ruta de la imagen entre paréntesis, precedidos de un signo de exclamación: ![Alt text](/ruta/de/la/img.jpg).

# Enlaces

* [Markdown - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Markdown)
* [Embellecer el contenido de la tarjeta con formato MarkDown](https://trello.com/c/cxWWIhJf/14-embellecer-el-contenido-de-la-tarjeta-con-formato-markdown-tarjetas-trello-markdown-texto).
* [Add formatting to your card descriptions using markdown on Trello ...](https://trello.com/c/JmIuOCpq/95-add-formatting-to-your-card-descriptions-using-markdown).
* [Cómo formatear tu texto en Trello](https://help.trello.com/article/821-using-markdown-in-trello).
* [Daring Fireball. Markdown: Syntax](https://daringfireball.net/projects/markdown/syntax): Sintaxis completa MarkDown.

Aplicaciones y editores Markdown:


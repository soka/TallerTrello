<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Editar tarjetas](#editar-tarjetas)
- [Menús rápidos](#men%C3%BAs-r%C3%A1pidos)
- [Arrastrar y soltar archivos adjuntos](#arrastrar-y-soltar-archivos-adjuntos)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Editar tarjetas

Para editar un tarjeta, haz clic sobre la tarjeta de del tablero que quieras editar. Si quieres editar el título de una tarjeta, haz clic sobre él. Las tarjetas también incluyen un campo de descripción opcional. Introduce un texto en la descripción para añadirle más detalles a la tarjeta. Si quieres, puedes utilizar [Markdown](/doc/chapter-trello-web/cards/markdown/markdown.md) para dar formato al texto de la descripción.

Abre un tarjeta para editar el título y la descripción. Si alguien más está editando la descripción de la tarjeta mientras estás en la tarjeta, Verás su avatar con un lápiz al lado de él.

# Menús rápidos

Pasa el ratón sobre un tarjeta y haz clic en el icono del lápiz que aparecerá encima de la tarjeta. Dentro del menú de la tarjeta podrás realizar una serie de acciones en un tarjeta directamente desde la vista del tablero.

![](/doc/chapter-trello-web/cards/editar-tarjetas/images/menu-rapido-tarjeta.png)

# Arrastrar y soltar archivos adjuntos

Puedes arrastrar y soltar varios archivos desde tu escritorio a una tarjeta para subirlos. También puedes arrastrar imágenes desde otros sitios web a tus tarjetas.

# Enlaces

* help.trello.com [Editar tarjetas](https://help.trello.com/article/784-editing-cards).
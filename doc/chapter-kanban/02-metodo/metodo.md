<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Método Kanban](#m%C3%A9todo-kanban)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Método Kanban

La palabra **kanban** se usa a menudo a lo largo de este capítulo pero no en todas las ocasiones se escribe en mayusculas, en el capítulo anterior nos hemos referido en todo momento a la palabra Japonesa **kanban** empleado dentro del proceso de producción de Toyota para **controlar el trabajo en curso** y referido a las tarjetas físicas con información o a tableros visuales. Estos sistemas fueron una de las muchas inspiraciones que están detrás del **Método Kanban**.

El **Método Kanban** escrito en con la primera letra en mayuscula tiene su origen en el año 2007 de una serie de metologías de gestión ideadas por **David J. Anderson** mientras trabajaba en Microsoft y [Corbis](https://en.wikipedia.org/wiki/Branded_Entertainment_Network).

Es importante **diferenciar entre el método que utilizamos y el sistema sobre el que actuamos**. Muchos equipos piensan que estan haciendo Kanban cuando en realidad sólo usan un tablero visual sin imponer ninguna de la prácticas básicas del método, a esto se de suele demoninar un sistema **protokanban**. El simple uso de un tablero visual no significa que ya estemos usando una metodología ágil.

Esta metodología también la podemos enmarcar dentro de las **metodologías Ágiles o “Agile”**, que pretender reducir la burocracia en los proyectos y dar respuestas rápidas a la gestión del cambio o imprevistos asociados a cualquier proyecto.

# Enlaces 
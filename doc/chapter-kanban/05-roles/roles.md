<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Roles en Kanban](#roles-en-kanban)
  - [Gestor de Peticiones de Servicio (Service request Manager)](#gestor-de-peticiones-de-servicio-service-request-manager)
  - [Gestor de Prestación de Servicio (Service Delivery Manager)](#gestor-de-prestaci%C3%B3n-de-servicio-service-delivery-manager)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Roles en Kanban

## Gestor de Peticiones de Servicio (Service request Manager)

Es la persona que recibe las necesidades del cliente y se ocupa de ordenar y facilitar los elementos de trabajo. También se conoce como Product Owner.

## Gestor de Prestación de Servicio (Service Delivery Manager)


# Enlaces

* jeronimopalacios.com [Kanban](https://jeronimopalacios.com/kanban/).

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Kanban](#kanban)
- [Taiichi Ohno y Toyota, un poco de historia](#taiichi-ohno-y-toyota-un-poco-de-historia)
- [Producción push VS pull](#producci%C3%B3n-push-vs-pull)
- [Un ejemplo actual de un sistema kanban: Starbucks](#un-ejemplo-actual-de-un-sistema-kanban-starbucks)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Kanban

![](/doc/chapter-kanban/01-intro/images/KanBan.png)

El término **kanban** existe tanto en Chino como en Japonés, 

* kanban escrito en **Chino** (看板) significa **señal o tablero visual**. Literalmente significa “Mirando al tablero”. A pesar de que los kanjis se utilizan tanto en Japonés como en Chino, en Chino sólo existe ese significado.
* kanban escrito en **Japonés** (かんばん) significa **tarjeta o señal**. Por ejemplo, una tarjeta que te entregan al entrar a un probador que indica el número de prendas que portas para que el dependiente pueda saber con cuantas sales.

![](/doc/chapter-kanban/01-intro/images/Kanban-on-Store.jpg)

Imagen: kanban de una tienda de moda en Ginza, Tokyo.

# Taiichi Ohno y Toyota, un poco de historia

![](/doc/chapter-kanban/01-intro/images/Taiichi_Ohno_01.jpg)

[Taiichi Ohno](https://es.wikipedia.org/wiki/Taiichi_Ohno) (1912-1990) fun un ingeniero industrial Japonés. Es conocido por diseñar el sistema de producción Toyota, Just In Time (JIT), dentro del sistema de producción de fabricación de automóviles.

![](/doc/chapter-kanban/01-intro/images/kanban-board-toyota.png)

La metodología **Kanban** fue desarrollada en Toyota por [Taiichi Ohno](https://es.wikipedia.org/wiki/Taiichi_Ohno) a principios de los años 40 dentro del sistema de gestión de producción JIT (just-in-time). 

Se utilizaba para controlar el avance del trabajo, en el contexto de una línea de producción. El objetivo era rentabilizar los suministros disponibles para la cadena de montaje de forma que no fueran demasiado excesivos ni tan escasos que la cadena se tuviera que parar por falta de recursos.

Mediante tarjetas o carteles visuales les permitia dividir el proceso productivo en fases bien delimitadas. Por lo general, se trata de tarjetas u hojas de papel sencillas que se adhieren a un lote de material.

![](/doc/chapter-kanban/01-intro/images/Toyota-Kanban-Cropped.jpg)

La imagen superior es un ejemplo de tarjeta kanban original para el envío de ejes y cojinetes de JTEKT (fabrica de componentes de automoción) a la planta de Toyota en la ciudad de [Takaoka](https://es.wikipedia.org/wiki/Takaoka_(Toyama)).  La tarjeta impresa tiene aproximadamente un tercio del tamaño de una DINA4 y se adjuntaba al material en producción, cuando era necesaria se imprime una nueva. Podemos diseccionar los elementos de información que contiene la tarjeta de la imagen:

![](/doc/chapter-kanban/01-intro/images/Toyota-Kanban-Area-Locations.jpg)

Por ejemplo el código "42450-12200-0" indica que pieza es (un eje de una rueda), número de unidades, fabrica de origen y otros muchos datos.  

El siguiente es un modelo más antiguo sin identificativo QR y con menos información:

![](/doc/chapter-kanban/01-intro/images/Toyota-Triangle-kanban.jpg)

![](/doc/chapter-kanban/01-intro/images/portada-libro-kanban-jit-toyota.png)


# Producción push VS pull

Tradicionalmente en los procesos de producción se fabrica para crear un stock ideal basado en la previsión de la demanda del cliente final, esto es lo que se conoce como un sistema **push**, esto generalmente produce en grandes lotes y cantidades lo que supone una gran cantidad de dinero en material inacabado en curso y el espacio necesario de almacenaje (también es dinero).   

**En un sistema kanban el principio "pull" (arrastre o tirón) es inherente al mismo**, nuevas entradas da material sólo se activan cuando el cliente pide un producto, esto envía una señal en sentido inverso en la cadena de producción para activar la fabricación. 

Por ejemplo: 

* En un supermercado se repone una estantería vacia cuando un producto falta.   
* Cuando un parquing está lleno sólo se permite entrar un nuevo coche cuando otro estacionado abandona la plaza. 

**En el mundo real no es tan fácil**, implica fabricar un producto de forma económica y rápida, a menudo las máquinas industriales pueden trabajar con diferentes tipos de materiales o piezas, existe un tiempo de preparación de la máquina para cada pieza determinada por eso se tiende a generar varias por lotes para minizar el gasto económica de tiempo cambiando de un proceso a otro.

Ejemplo: Cadena de clips.

# Un ejemplo actual de un sistema kanban: Starbucks

A pesar de que los sistemas kanban tuvieron origen en las empresas de manufacturación se puede encontrar en diferentes contextos muy variados hoy día.  

Un ejemplo real es el de las cafeterías Starbucks, cuando nos ponemos a la cola y pedimos un café sucede los siguiente. 

La descripción del flujo de trabajo es el siguiente:

* La persona que nos atiende recoge nuestras preferencias en un lateral de la taza, el tipo de cafe, la leche, opciones adicionales y nuestro nombre. De alguna manera está generando una orden de trabajo y la información visual viaja junto con el pedido. El dependiente deja la taza junto a la máquina de café.
* Cuando la persona que hace el café (el [barista](https://es.wikipedia.org/wiki/Barista)) está libre (cuando hay varios pedidos se puede formar una cola) coge la siguiente taza vacia prepara el café y lo deja en la zona de expedición.
* Finalmente una tercera persona se ocupa de llamar al cliente por el nombre escrito en la taza y entregarle el pedido.

![](/doc/chapter-kanban/01-intro/images/starbucks-cup.jpg)

En los horarios de más ajetreo puede pasar que se formen colas de tazas esperando a ser preparadas, para aliviar el cuello de botella se puede usar una segunda máquina o que una persona de caja ayude a la persona que lo prepara.

Una métrica básica para medir la productividad es medir el tiempo de ciclo de vida del cliente, el tiempo que tardá desde que pide un café hasta que lo recibe en su mano.

Otros ejemplos de la vida real que podemos encontrar son la reposición de las máquinas autoventa de comida o las hamburgeserias de McDonald's.


# Enlaces

* [Kanban • Jerónimo Palacios](https://jeronimopalacios.com/kanban/).
* [Empezar con el método Kanban, guía de cinco pasos](https://jeronimopalacios.com/2016/08/checklist-para-empezar-con-el-metodo-kanban/).
* [El método Kanban](http://blog.jmbeas.es/2016/06/12/el-metodo-kanban/).
* [Metodología Kanban | Kanban Tool](https://kanbantool.com/es/metodologia-kanban).
* [Kanban - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Kanban)
* [Kanban (desarrollo) - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Kanban_(desarrollo))
* [Kanban • Jerónimo Palacios](https://jeronimopalacios.com/kanban/)
* allaboutlean.com [Anatomy of the Toyota Kanban](https://www.allaboutlean.com/toyota-kanban/).
* allaboutlean.com [Kanban Card Design](https://www.allaboutlean.com/kanban-card-design/).
* [Kanban & pull system (Sistema Jalón)(https://www.leansolutions.co/conceptos/kanban/).
* [Sistema Kanban y control de inventario Pull](https://www.kanban-system.com/es/sistema-kanban-y-control-de-inventario-pull/).
* [Coffee Shop Kanban: Is your dev team a Starbucks or a Costa?](https://blog.patchspace.co.uk/coffee-shop-kanban).
* [McDonald's Lean Food Preparation System - Made for You](http://www.leansimulations.org/2012/04/mcdonalds-lean-food-preparation-system.html).
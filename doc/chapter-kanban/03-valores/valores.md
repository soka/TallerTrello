<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Valores Kanban](#valores-kanban)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Valores Kanban

**El Método Kanban está guiado por valores**, según David J.Anderson son siguientes: Transparencia, equilibrio, foco en el cliente, flujo, liderazgo, entendimiento, acuerdo y respeto.

Trello y otros tableros kanban digitales hacen suyos algunos de estos valores de forma natural, algunos ejemplos son las menciones a otros usuarios (@) invitando a la colaboración en una tarjeta o la revisión de un tablero por parte del equipo del proyecto de forma periódica para realizar un seguimiento de un proyecto, normalmente todos los tableros son visibles para todos los miembros del equipo y pueden consultar cualquier tarjeta. Permite que todos los miembros del equipo tengan una **visión global, común y compartida de un proyecto**. 


# Enlaces 
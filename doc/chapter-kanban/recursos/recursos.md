<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Kanban Esencial Condensado - David J Anderson y Andy Carmichael](#kanban-esencial-condensado---david-j-anderson-y-andy-carmichael)
- [](#)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Kanban Esencial Condensado - David J Anderson y Andy Carmichael

![](/doc/chapter-kanban/recursos/images/essential_kanban_condensed_cover.jpg)

PDF desgargable de forma gratuita del siguiente [enlace](https://leankanban.com/essential-kanban-condensed-spanish/).

Muchos de los contenidos de este taller estan sacados de este libro que condensa o destila las ideas clave del Método Kanban.


# 


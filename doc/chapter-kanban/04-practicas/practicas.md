<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Prácticas](#pr%C3%A1cticas)
  - [WiP: Limitar el trabajo en curso](#wip-limitar-el-trabajo-en-curso)
  - [Visualizar el trabajo](#visualizar-el-trabajo)
  - [Gestionar el flujo](#gestionar-el-flujo)
  - [Explicitar políticas](#explicitar-pol%C3%ADticas)
- [Enlaces:](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Prácticas

## WiP: Limitar el trabajo en curso 

El número máximo de tareas que se pueden realizar en cada fase debe ser algo conocido. 

Las políticas que fijan el **WiP** crean un **sistema pull**, cuando un trabajo acaba (la tarjeta sale del sistema) deja hueco para que una nueva tarea sea "arrastrada" dentro del flujo de trabajo. 

![](/doc/chapter-kanban/04-practicas/images/wip_libro_anderson.png)

El concepto **multitarea en Kanban** se refiere a cuando un miembro del equipo tiene en ejecución más de una tarea, es un error pensar que la productividad de una persona si alterna entre varias tareas a lo largo de la jornada será la misma que si se dedica a ellas de forma secuencial. Simplemente cambiar de contexto entre tareas supone un tiempo adicional de "aclimatación". 

![](/doc/chapter-kanban/04-practicas/images/tiempo-perdido-entre-tareas.jpg)

**Ejercicio (context switching)**: En un papel dibuja una tabla con tres columnas y diez filas. Cada columna representa una tarea que debes realizar y cada fila es una parte de las acciones correspondientes a cada una de las tareas. La primera tarea consiste en que en las celdas de la primera columna escribas los números del 1 al 10. Para la segunda tarea haz lo mismo con las letras de la A a la J. Y, por último, en la tercera tarea tienes que escribir en números romanos del I al X. En una hoja realiza las tres tareas de manera secuencial. La primera tarea, la segunda y, finalmente, la tercera. A continuación, en otro papel realiza las tres tareas en modo multitarea, es decir, por filas. La primera fila será 1, A, I; la segunda será 2, B, II; y así sucesivamente hasta completar las diez filas.

## Visualizar el trabajo

![](/doc/chapter-kanban/04-practicas/images/Simple-kanban-board.jpg)

Ejemplo [TallerTrello - Ejemplo Kanban básico](https://trello.com/b/nky9PJw5).

Un **tablero kanban** es una manera de visualizar el trabajo y el proceso que sigue en su ciclo de vida. El solo hecho de **visualizar un proceso** debe ser el **resultado de la colaboración del equipo para entender y explicitar la forma de trabajo** realizando mejoras continuas sobre el mismo.  

El **Método Kanban no es un sistema rígido** y no impone como diseñar los tableros. Normalmente las columnas o listas como las llaman en Trello representan etapas de un proceso, las columnas pueden estar atravesadas horizontalmente (calles si atraviesan varias columnas) para representar estados dentro de cada fase. Es muy común dejar una calle para los trabajos urgentes o prioritarios. 

El diseño visual de la tarjeta es también esencial, podemos usar colores por ejemplo para identificar a quien pertecenen, la gravedad del asunto o la clase de servicio (funcionalidades, mejoras del sistema, fallos...).

Pongamos un ejemplo sencillo de modelizar como un flujo de trabajo, el diagrama que se muestra a continuación esta obtenido de Internet y muestra el flujo de aprobación de una factura de una empresa. 

![](/doc/chapter-kanban/04-practicas/images/Invoice-customer-process.png)

Podemos imaginar cada caja del diagrama como un estado del proceso, cada nueva factura generará una nueva tarjeta y viajará por las diferentes columnas de izquierda a derecha normalmente. 

Enlace al ejemplo en Trello: [TallerTrello - Ejemplo ciclo aprobación factura](https://trello.com/b/BQeZWehB).

A simple vista se observa una de las "debilidades" del sistema kanban, está pensado para procesos secuenciales o líneales, una factura rechazada puede ser modificada y volver al paso inicial.

## Gestionar el flujo

En el Método Kanban se percibe el **trabajo como un flujo de valor** (Lean flow). 

**Aplicar métricas e inspeccionar el flujo de trabajo** es indispensable para detectar cuellos de botella o tareas críticas que pueden bloquear el avanze del resto. 

Indicadores o métricas de ejemplo: 

* El **Lead Time** o tiempo de entrega de una tarea es clave para medir el ritmo de trabajo. 
* **Cost of Delay** (CoD): El coste del retraso de la entrega de una tarea.

## Explicitar políticas 

Las **políticas** deben ser simples y visibles en el sistema o tablero kanban. Deberían aplicarse en todos los casos como por ejemplo el límite WiP por columna. En general las políticas deben permitir **tomar una decisión rápido y sin consultar**. 

![](/doc/chapter-kanban/04-practicas/images/politicas.png)

Ejemplos:

* **Políticas para realimentar el sistema** con una nueva tarea. En el método Kanban no se selecciona el trabajo dependiendo de su orden de llegada, sino dependiendo de su clase de servicio y cualquier otra política expresa
* **Clases de servicio**: Las clases de servicio son una posible clasificación de las tareas. 

# Enlaces:

* ["‘Personal kanban’: un método para sobrevivir a la multitarea"](https://www.yorokobu.es/kanban/).
* ["Multitarea en Kanban. Los tiempos perdidos en tu proyecto"](https://jorgesaiz.com/blog/multitarea-en-kanban-tiempos-perdidos/).
* ["'Personal kanban', el nuevo método de moda para sobrevivir a la multitarea"](http://theobjective.com/further/personal-kanban-el-nuevo-metodo-de-moda-para-sobrevivir-a-la-multitarea/).
* ["Como lograr el mejor WIP de un KANBAN"](http://www.javiergarzas.com/2012/04/el-mejor-wip-de-un-kanban.html).
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Técnica Pomodoro](#t%C3%A9cnica-pomodoro)
- [Mecánica básica](#mec%C3%A1nica-b%C3%A1sica)
- [Aplicaciones Pomodoro](#aplicaciones-pomodoro)
  - [PomoDoneApp: Pomodoro Timer for Trello](#pomodoneapp-pomodoro-timer-for-trello)
  - [Marinara Timer](#marinara-timer)
  - [TomatoTimer](#tomatotimer)
- [Conclusiones](#conclusiones)
- [Enlaces externos](#enlaces-externos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Técnica Pomodoro 

![](/doc/chapter-productividad/06-pomodoro/images/01.png)

La técnica Pomodoro (“tomate”) fue desarrollada por [**Francesco Cirillo**](https://francescocirillo.com/pages/francesco-cirillo) [@cirillof](https://twitter.com/cirillof) a fines de la década de 1980. El nombre está inspirado en los temporizadores de cocina con forma de tomate. 

La técnica trata de conseguir que se logren tantas tareas como sea posible en la menor cantidad de tiempo, manteniendo una concentración alta (sin distracciones de ningún tipo) mientras el cerebro está fresco y descansado.  El objetivo es **administrar el tiempo** dedicado a una actividad en intervalos llamados *pomodoros* de 25 minutos con descansos de 5 minutos intercalados y pausas más largas cada 4 *pomodoros*. 

![](/doc/chapter-productividad/06-pomodoro/images/como-usar-la-tecnica-pomodoro1.jpg)

# Mecánica básica 

Para comenzar a aplicar la técnica Pomodoro sólo necesitas un relojo temporizador, incluso mejor si es uno físico y sencillo como el que inspiro a Cirillo. Al principio o final del día planifica las tareas que quieres acometer y apuntalas en una hoja o en una lista de Trello diaria por ejemplo.

* Decidir la tarea o actividad a realizar. ¡Midela en Pomodoros!.
* Poner el temporizador, situalo de forma que puedas ver siempre el tiempo que queda. 
* Trabajar en la tarea de manera intensiva hasta que el temporizador suene, incluye algunos minutos para recapitular y revisar lo que hiciste.
* Hacer una marca para anotar qué pomodoro se ha completado. 
* Tomar una pausa breve, a veces levantarte de la silla y moverse un poco ayuda a cambiar de escenario mental.
* Cada cuatro pomodoros, tomar una pausa más larga

# Aplicaciones Pomodoro

Aunque podemos seguir usando un temporizador de cocina por su simpleza hoy día existen aplicaciones digitales especializadas. 

## PomoDoneApp: Pomodoro Timer for Trello

![](/doc/chapter-productividad/06-pomodoro/images/pomodoneapp_01.png)

[PomoDoneApp](https://my.pomodoneapp.com/) es una aplicación Web que se integra con Trello, debemos abrir una cuenta gratuita y conectar la aplicación con un tablero de nuestra elección, en mi caso usare el [tablero principal](https://trello.com/b/5Q7Rcbqc) donde estoy preparando este taller. 

Las tarjetas de Trello se sincronizan con PomoDoneApp y podemos iniciar slots de trabajo de 5, 15 o 25 minutos. 

![](/doc/chapter-productividad/06-pomodoro/images/pomodoneapp_02.png)

## Marinara Timer

![](/doc/chapter-productividad/06-pomodoro/images/marinaratimer_01.png)

![](/doc/chapter-productividad/06-pomodoro/images/marinaratimer_02.png)

## TomatoTimer 

[TomatoTimer](https://tomato-timer.com/) también se basa en Web, una de sus características destacables es su interfaz minimalista y sus atajos de teclado. Para algunos navegadores también podemos habilitar notificaciones que nos avisen con un sonido.

![](/doc/chapter-productividad/06-pomodoro/images/tomatotimer_01.png)

# Conclusiones

Existen integraciones entre Trello y aplicaciones para Pomodoro, por desgracia Trello en su versión gratuita no tiene time-tracking nativo para implementar la técnica Pomodoro sin necesidad de otras aplicaciones.

# Enlaces externos

* Wikipedia [Técnica Pomodoro - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/T%C3%A9cnica_Pomodoro).
* Web oficial [Francesco Cirillo](https://francescocirillo.com/).
* Perfil Twitter [@cirillof](https://twitter.com/cirillof?lang=es). 
* [LA MEJOR FRUTA PARA APROBAR EXÁMENES, CONSEGUIR AUMENTOS, Y TENER MÁS TIEMPO](https://borcobritas.com/la-tecnica-pomodoro/).
* [How To Pomodoro Your Way To Productivity [Video Tutorial]](https://blog.trello.com/how-to-pomodoro-your-way-to-productivity).
* [Cómo usar la técnica Pomodoro para aumentar tu productividad](https://blogthinkbig.com/como-usar-la-tecnica-pomodoro). 
* [La Técnica Pomodoro, a fondo - FacileThings](https://facilethings.com/blog/es/pomodoro-technique-details).
* [Introducing Trellodoro – Trello + Pomodoro = Personal Kanban Bliss ...](https://dillieodigital.wordpress.com/2014/06/10/introducing-trellodoro-trello-pomodoro-personal-kanban-bliss/).
* [No distractions! Achieve more with Trello and Pomodoro combined](https://medium.com/@vladcampos/no-distractions-achieve-more-with-trello-and-pomodoro-combined-1c65bf7a6ed6).
* [Técnicas de estudio: La Metodología de Pomodoro](http://noticias.universia.es/educacion/noticia/2017/12/27/1157076/tecnicas-estudio-metodologia-pomodoro.html).
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Lo urgente y lo importante: un secreto del ex presidente Eisenhower](#lo-urgente-y-lo-importante-un-secreto-del-ex-presidente-eisenhower)
- [¿Qué es Urgente?](#%C2%BFqu%C3%A9-es-urgente)
- [Como identificar las cosas importantes de la vida](#como-identificar-las-cosas-importantes-de-la-vida)
- [La matriz](#la-matriz)
  - [Urgente e importante](#urgente-e-importante)
  - [Importante, pero no urgente: Planifica](#importante-pero-no-urgente-planifica)
  - [Urgente, pero no importante](#urgente-pero-no-importante)
  - [Ni importante ni urgente: "Algún día tengo que ..."](#ni-importante-ni-urgente-alg%C3%BAn-d%C3%ADa-tengo-que-)
- [Conclusiones](#conclusiones)
- [Ponlo en práctica](#ponlo-en-pr%C3%A1ctica)
- [Enlaces externos](#enlaces-externos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Lo urgente y lo importante: un secreto del ex presidente Eisenhower 

> "Dicen que para lograr el éxito nunca debes olvidar cual es el objetivo"

Con el objetivo en mente debes **alinear tus decisiones** para su consecución.

![](/doc/chapter-productividad/03-eisenhower/images/01.png)

Conocemos a [Dwight D. Eisenhower](https://es.wikipedia.org/wiki/Dwight_D._Eisenhower) (1890 - 1969) por ser el trigésimo cuarto presidente de los EE.UU.

Vivio la segunda guerra mundial en vivo y en directo en el frente, tras la guerra fue jefe del estado mayor del ejercito y en 1952 se involucro en política de la mano del partido republicano. 

Su productividad es legendaria. Era considerado un maestro de la administración del tiempo; tenía la habilidad de hacer todo cuando era necesario hacerlo.

Pensaba que debemos dedicar atención y tiempo a nuestras actividades en función de su **importancia y urgencia**.

# ¿Qué es Urgente?

**Algo es urgente cuando requiere una atención inmediata.** La urgencia tiene que ver con la **temporalidad de la tarea** y con las **consecuencias que acarrea no hacerla**. Algo urgente puede se por ejemplo mandar un correo electrónico hoy porqué finaliza un plazo. 

> "Las urgencias son el peor enemigo de los resultados"

Hacen que interfieran con otras tareas que pudiesemos estar haciendo rompiendo la continuidad y con el coste añadido del tiempo necesario para cambiar entre una tarea y otra.

Algunas preguntas que te puedes hacer a ti mismo:

* ¿Es necesario que haga esta tarea ahora? 
* ¿Qué pasaría si no hiciera esta tarea? 

# Como identificar las cosas importantes de la vida

Algo es **Importante cuando contribuye a tus objetivos a medio y largo plazo**, a tu propósito de vida o misión.

Algunas preguntas que te puedes hacer para saber si algo es importante:

* ¿Es importante esta tarea para conseguir mi objetivo? 
* ¿Te hace estar más cerca de lo que realmente quieres conseguir?
* ¿Donde nos queremos ver en un tiempo? 

# La matriz 

![](/doc/chapter-productividad/03-eisenhower/images/02.png)

La matriz tiene cuatro categorías:

## Urgente e importante

Cuando algo sea urgente e importante **hazlo** inmediatamente. Significa que te acerca más a la consecución de tus objetivos y además corres algún riesgo de no hacerlo.

Conviene discernir bien que es realmente urgente e importante ya que tenderemos siempre a sobrecargar este cuadrante y al final terminamos agobiados por la presión de tener que abarcar todo.


## Importante, pero no urgente: Planifica

**Programa el tiempo** para hacerlo, genera un comportamiento más estratégico, orientado a objetivos a largo plazo. Para no desatender las actividades que son importantes, pero no son urgentes, necesitas **ser proactivo**, ya que éstas no demandan tu atención inmediata. Si las dejas de lado, llegará un momento en que se convertirán en urgentes, y este comportamiento te llevará al circulo vicioso que implica vivir siempre en modo reactivo, en una situación de crisis continua.

## Urgente, pero no importante

Delega. ¿?

Respecto a este punto recuerda lo que dijo Eisenhower: 

> “Liderazgo es el arte de lograr que alguien haga lo que tú quieres porque quiere hacerlo"


## Ni importante ni urgente: "Algún día tengo que ..."

Aplázalo o directamente borralo de tu mente, si querias hacerlo pero siempre lo aplazas probablemente no sea vital, más adelante tal vez vuelva a surgir en otras condiciones y puedas acomoterlo, reduce el ruido.

# Conclusiones

El sistema de clasificación **te obliga a reflexionar** sobre la naturaleza de cada tarea, tal vez la hayamos juzgado superficialmente y algo que parecía urgente e importante no lo era.  

El truco **consiste en  distinguir entre lo urgente y lo importante**, y saber qué hacer con todos.

Lo más obvio sería "atacar" las tareas urgentes e importantes (genera un comportamiento reactivo ante lo inmediato), sin embargo es primordial dejar un espacio de tiempo para avanzar en lo importante pero no urgente. 

Con el objetivo siempre en mente debes tomar buenas decisiones, o tratar de hacerlo al menos, de las que parecen más insignificantes o nimias hasta las mas significativas, nos enfrentamos a cientos o miles de decisiones al día, desde como vestirnos por la mañana hasta que nos acostamos pasando por el propio trabajo por eso hay gente que decide reducir al mínimo las que hacen "ruido" (por ejemplo vestir ropa similar). 

Una de las características es la **facilidad y rapidez de uso** de la matriz, **vale para cualquier situación**, a nivel profesional por ejemplo para valorar un proyecto o en el ámbito personal.  

Tambien puede ser una buena **herramienta visual** que nos ayude a centrarnos un poco y focalizar sobre una cuestión concreta.

**La elección de una tarea sobre otra no puede basarse en criterios emotivos o de “ganas”**. No puede ser el azar, ni hacer lo que nos apetece más en ese momento.

La urgencia tiende a aumentar a medida que se acerca la fecha límite. Es importante saber si la tarea tiene una fecha límite para poder programarla, más aún si esta tarea puede llevarnos varios días en completar. Una tarea grande a veces se puede simplificar, dividir en tareas más pequeñas y volver a valorarlas. Ta facilitará empezar a trabajar en ella.

Las tareas a menudo o casi siempre no suelen venir "solas", **una tarea es crítica cuando bloquea la realización de otras tareas**. El sistema de la matriz de Eisenhower es simple y no da respuesta para gestionar esta compleja red de tareas interrelacionadas.

# Ponlo en práctica

![](/doc/chapter-productividad/03-eisenhower/images/04.jpg)

En Trello no podemos dar forma a la propia matriz, las listas se muestran ordenadas en horizontal, en cualquier caso podemos crear una lista para cada una de las combinaciones posibles, el resultado sería similar al de la imagen inferior.

![](/doc/chapter-productividad/03-eisenhower/images/03.png)

[Enlace al tablero](https://trello.com/b/U7LlFyvG).

# Enlaces externos

* animalpolitico.com [Lo urgente y lo importante: un secreto del legendariamente productivo expresidente Eisenhower](https://www.animalpolitico.com/2017/07/urgente-eisenhower-tiempo-matriz/).
* adriansanchez.es [CÓMO MEJORAR TU PRODUCTIVIDAD CON LA MATRIZ EISENHOWER](https://adriansanchez.es/como-mejorar-tu-productividad-con-la-matriz-eisenhower/).
* blog.almadark.com [Lo urgente y lo importante, matriz de Eisenhower](http://blog.almadark.com/workflow/20160522/lo-urgente-y-lo-importante-matriz-de-eisenhower/).
* Trello [Eisenhower Matrix](https://trello.com/b/saM55Fu8/eisenhower-matrix).
* [The Eisenhower Matrix Productivity Tool As A Trello Board](https://medium.com/@trello/the-eisenhower-matrix-productivity-tool-as-a-trello-board-520449536b2e). 
* [Prioritize Your Tasks Using General Eisenhower’s Matrix with Trello](https://shift.newco.co/prioritize-your-tasks-using-general-eisenhowers-matrix-with-trello-12536984650a).
* [The Eisenhower Matrix Productivity Method [Video Tutorial]](https://blog.trello.com/eisenhower-matrix-productivity-tool-trello-board).
* [Dwight D. Eisenhower - Wikipedia, la enciclopedia libre](https://es.wikipedia.org/wiki/Dwight_D._Eisenhower)
* [La Matriz de Gestión del Tiempo - FacileThings](https://facilethings.com/blog/es/time-management-matrix).
* [To Dos: Eisenhower Matrix Style](https://trello.com/b/CXVefGzw/to-dos-eisenhower-matrix-style).
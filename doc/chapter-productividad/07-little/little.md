<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [La Ley de Little](#la-ley-de-little)
  - [Más sobre la teoría de la colas](#m%C3%A1s-sobre-la-teor%C3%ADa-de-la-colas)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# La Ley de Little

![](/doc/chapter-productividad/07-little/images/John_Little.jpeg)

La **ley de Little** fue enunciada en 1961 por el profesor del MIT [John D.C Little](https://en.wikipedia.org/wiki/John_Little_(academic)). El origen de la ley es la [teoría de las colas](https://es.wikipedia.org/wiki/Teor%C3%ADa_de_colas).

La ley demuestra las relaciones entre el tiempo de espera (Lead Time), el Trabajo en curso (WIP) y el Rendimiento (Throughput).

![](/doc/chapter-productividad/07-little/images/Little_s_Law.png)


## Más sobre la teoría de la colas



# Enlaces

* [La Ley de Little | Berriprocess](http://old.berriprocess.com/es/todas-las-categorias/item/46-ley-de-little).
* [La ley de Little, y las gallinas que entran por las que van saliendo](https://vieiro.net/blog/20140320.html).
* Video [La ley de Little aplicada a un sistema de fabricación](https://www.youtube.com/watch?v=26V50LgipLg).
* [John Little (academic) - Wikipedia](https://en.wikipedia.org/wiki/John_Little_(academic)).
* [Artículo publicado por John Little y Stephen C. Graves](http://web.mit.edu/sgraves/www/papers/Little's%20Law-Published.pdf), en 2008, donde se explica la ley de forma formal, pero fácil de leer y divertido.
* [LEY DE LITTLE](https://www.saraclip.com/ley-de-little/).
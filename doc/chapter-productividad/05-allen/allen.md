<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Organízate con eficacia, David Allen](#organ%C3%ADzate-con-eficacia-david-allen)
- [Administrar el flujo de trabajo](#administrar-el-flujo-de-trabajo)
  - [Recopilación](#recopilaci%C3%B3n)
  - [Procesamiento](#procesamiento)
  - [Organización](#organizaci%C3%B3n)
    - [Proyectos](#proyectos)
- [Ejemplo práctico en Trello](#ejemplo-pr%C3%A1ctico-en-trello)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Organízate con eficacia, David Allen

![](/doc/chapter-productividad/05-allen/images/cover-gtd.jpg)

**"Organízate con eficacia"** (en ingles ["Getting Things Done: The Art of Stress-Free Productivity"](https://www.amazon.es/Getting-Things-Done-David-Allen/dp/0143126563)) es una publicación de [David Allen](https://es.wikipedia.org/wiki/David_Allen) que trata de dar respuesta preguntas como: 

* *qué* hacer, 
* *cuándo* hacerlo 
* y *cómo* hacerlo. 

La premisa básica del autor es que **nuestra productividad está directamente relacionada con nuestra habilidad para relajarnos** (estado de *mente como el agua*). Solo cuando nuestras mentes piensan con claridad y nuestros pensamientos estan organizados podemos desarrollar todo nuestro potencial.

El autor define nuestro estado ideal como **mente como el agua**, si mantenemos todas las tareas en la cabeza es dificil coger el control y relajarnos. El número de cosas sin resolver que podemos albergar en nuestra cabeza es limitada. A menos que vaciemos la "RAM" de nuestras preocupaciones estas volveran una y otra vez, ese es un comportamiento poco eficaz. 

![](/doc/chapter-productividad/05-allen/images/Anxiety-David-Kekich.jpg)

El método se basa en **dos objetivos** clave:

1. **Captar todas las cosas que hay que hacer** —ahora, más adelante, algún día...- y registrarlas en un sistema para que nuestra mente no tenga que cargar con ellas.
2. Implementar una **metodología para la toma de decisiones**.  

![](/doc/chapter-productividad/05-allen/images/brilliantpotshot.gif)

Imagen: [Ashleigh Brilliant](https://en.wikipedia.org/wiki/Ashleigh_Brilliant) dibujante.

# Administrar el flujo de trabajo 

David Allen simplifica la toma de decisiones en cinco pasos:

1. **Recopilamos** todo lo que nos preocupa o creemos que está incompleto.
2. **Procesamos** su significado y lo que debemos hacer con ellas.
3. **Organizamos** los resultados.
4. **Evaluamos** las opciones para determinar que acometer.
5. **Hacer** lo que nos hemos propuesto.

![](/doc/chapter-productividad/05-allen/images/mapa-getting-things-done.png)

## Recopilación

![](/doc/chapter-productividad/05-allen/images/iStock_000004729175Small.jpg)

El primer paso consiste en **capturar TODO** lo que se debe hacer o sea susceptible de convertirse en una acción. Cualquier cosa o tarea incompleta por pequeña que parezca, temas personales y laborales que sentimos debemos cambiar, todo lo que responda a un debería, he de, tendría que, voy a... Todo este inventario lo recogemos en "contenedores" y preferiblemente en algún lugar que no sea nuestra cabeza.  Las herramientas de recopilación pueden abarcar desde una libreta o una bandeja de oficina hasta soportes digitales. También se incluyen en este fase todos los asuntos en progreso.

Criterios:

1. Vaciar la cabeza y volcar las tareas pendientes en el sistema de recopilación. Las herramientas de recopilación deberían formar parte de nuestro estilo de vida, deberiamos tenerlas a mano en cualquier momento para recoger cualquier idea valiosa en cualquier situación. Es probable que las mejoras ideas se nos ocurran cuando no estamos trabajando.
2. Minimizar el número de contenedores de recopilación. Si tenemos demasiadas zonas de recopilación puede ser dificil de procesar.
3. Vaciar los contenedores periódicamente. "Vaciar" no significa que se deba hacer todo precisamente ahora sino que se debe examinar periódicamente.

## Procesamiento

En esta fase lo que debemos preguntarnos es: ¿Realmente tenemos que hacer algo al respecto?¿Requiere acción? Sólo hay dos respuestas posibles, Sí y No.

Si la respuesta es **NO requiere acción**, caben tres posibilidades:

1. Es desechable, innecesario. ¡Sólo necesitamos la tecla <<SUPR>>!
2. No es necesario actual ahora pero habría que hacer algo más tarde. 
3. Elemento de consulta con información de utilidad.

Para los dos últimos puntos necesitamos un archivo o agenda para material que se está incubando.

Si el elemento ** SÍ requiere acción** debemos hacernos varias preguntas de cada elemento:

* ¿Cual es el resultado o objetivo esperado?
* ¿Cual es la acción siguiente que requiere? Es la siguiente actividad física para avanzar hacia el objetivo final.

Una vez decidida cual va a ser la siguiente acción tenemos tres opciones:

1. **Hacerlo**: Si la acción nos va a llevar menos de dos minutos deberiamos hacerla en este mismo momento.
2. **Delegarlo**: Si la acción nos va a llevar menos de dos minutos debemos preguntarnos si somos la persona indicada para llevarla a cabo. Si la respuesta es no delegamos.
3. **Posponerlo**: Si somos la persona idonea para hacerlo pero nos va a llevar más de dos minutos debemos posponerla para más adelante en una lista de acciones pendientes.

## Organización

**Categorías** resultantes de los elementos procesados:

* Elementos que no requieren acción: Desechar, herramientas de incubación de material de consulta o <<archivo de seguimiento>> para volver a estudiarl más adelante.
* Elementos sujetos a acción. Necesitamos una lista de proyectos , un lugar donde almacenaro archivar los planes y materiales de proyectos, una agenda, una lista de recordatorios de las próximas acciones y otra de recordatorios de cosas que usted espere.

### Proyectos

¿Qué es un [proyecto]?  



# Ejemplo práctico en Trello

[TallerTrello - Ejemplo GTD](https://trello.com/b/i3I8BDs3).

# Enlaces

* [Resumen del libro - Organízate con eficacia GTD](https://www.leadersummaries.com/ver-resumen/organizate-con-eficacia-gtd).
* [Organízate con eficacia, David Allen](https://jordisanchez.info/libros-de-productividad/organizate-con-eficacia-david-allen/).
* [La Guía Definitiva de GTD - FacileThings](https://facilethings.com/blog/es/ultimate-guide-to-gtd).
* [8 Consejos para Implementar GTD](https://facilethings.com/blog/es/implement-gtd).
* Web oficial [Getting Things Done® - David Allen's GTD® Methodology](https://gettingthingsdone.com/).
* [GTD Básico: Vaciar la Bandeja de Entrada](https://facilethings.com/blog/es/basics-empty-inbox).
* [HOW TO WEED WACK YOUR INBOX DOWN TO ZERO](https://gettingthingsdone.com/2009/06/how-to-weed-wack-your-inbox-down-to-zero/).

Trello y GTD:

* [Trello con el método GTD implementar la productividad GTD en Trello](https://www.jesusbedmar.es/curso/trello-gestor-proyectos-ciclo/trello-con-el-metodo-gtd-es-imposible/).
* [Implementando GTD en Trello - Hipertextual](https://hipertextual.com/archivo/2012/12/gtd-trello/).
* [The GTD Approach To Maximizing Productivity With Trello - Trello Blog](https://blog.trello.com/gtd-getting-things-done-maximizing-productivity-trello).
* [TABLERO PUBLICO TRELLO CON LISTAS GTD](https://trello.com/b/XwU136Vo/tablero-publico-trello-con-listas-gtd).
* [¿Cómo utilizar Trello para la gestión de tareas mediante el método GTD?](http://www.albertogarciacarro.com/trello-gestion-de-tareas-metodo-gtd/).
* [New! GTD & Trello Setup Guide - Getting Things Done®](https://gettingthingsdone.com/2018/05/new-gtd-trello-setup-guide/).
* [La mente como agua](https://canasto.es/blog/2010/10/la-mente-como-agua).
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Mapas mentales](#mapas-mentales)
  - [¿Qué son y para qué sirvens](#%C2%BFqu%C3%A9-son-y-para-qu%C3%A9-sirvens)
  - [Características](#caracter%C3%ADsticas)
  - [Casos de uso](#casos-de-uso)
  - [Aplicaciones software](#aplicaciones-software)
  - [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Mapas mentales 

## ¿Qué son y para qué sirvens

Los mapas mentales es una **forma de representación gráfica**, las ideas parten de un nodo central y se van desplegando a través de nuevos nodos conectados con conceptos relacionados con su nodo raiz.

![](/doc/chapter-productividad/08-otros/images/mindmapfree_01.png)

Una definición más certera y extensa es la de Wikipedia, dice sobre el termino [Mapa mental](https://es.wikipedia.org/wiki/Mapa_mental):

> "Un **mapa mental** es un **diagrama** usado para **representar palabras, ideas u otros conceptos** ligados y dispuestos radicalmente alrededor de una palabra clave o de una **idea central**. Los mapas mentales son un método muy eficaz, para extraer y memorizar información. Son una forma lógica y creativa de tomar notas, organizar, asociar y expresar ideas, que consiste, literalmente, en cartografiar sus reflexiones sobre un tema, es representado por medio de dibujos o imágenes."

Normalmente las listas de tareas funcionan de una forma líneal o secuencial . Una de las propiedades a destacar de los mapas mentales es que **se asemeja mucho a la forma natural de como las personas procesamos la información**, imitan el funcionamiento natual del cerebro o como lo llaman el "pensamiento radial", esto hace que sean más intuitivos para asociar ideas o conceptos relacionados, funciona de forma asociativa.

Este [video corto de Philippe Boukobza](https://www.youtube.com/watch?time_continue=101&v=Vg862f-PFAQ) sintetiza de forma clara y concisa en 2 minutos en que consiste.

El gran difusor de la idea del mapa mental fue [Tony Buzan](https://es.wikipedia.org/wiki/Tony_Buzan) en 1974 con su libro *"Use Your Head"*.

## Características

## Casos de uso

Entre las posibilidades que ofrecen los mapas mentales a la hora de gestionar información, estarían entre otras las siguientes:

* Se puede aplicar a tareas creativas como una tormenta de ideas para un nuevo proyecto. 
* Facilitar la recogida y la síntesis de información antes de escribir un documento o artículo.
* **Organizar y visualizar la información** de manera más sintética.
* Mejorar la gestión de la información.
* **Facilitar la creatividad** en el diseño y la planificación de un proyecto.
* Sacar mayor provecho a las reuniones presenciales.

## Aplicaciones software 

## Enlaces 

Artículos:

* [Aumenta tu creatividad y productividad usando mapas mentales](https://www.ciudadano2cero.com/mapa-mental-productividad-creatividad/).
* [Los mapas mentales y la productividad](https://cincodias.elpais.com/cincodias/2014/08/05/economia/1407261962_777559.html).

Videos:

* [Video corto de Philippe Boukobza](https://www.youtube.com/watch?time_continue=101&v=Vg862f-PFAQ).


Aplicaciones:

* [MindMapfree](http://mindmapfree.com/).
* wwwhatsnew [3 apps para crear mapas conceptuales](https://wwwhatsnew.com/category/productividad/mapas-mentales/).
* [MINDMUP](https://www.mindmup.com/).
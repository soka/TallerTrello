<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Introducción](#introducci%C3%B3n)
- [Ley de Parkinson](#ley-de-parkinson)
- [¿Como solucionarlo?](#%C2%BFcomo-solucionarlo)
- [Conclusiones](#conclusiones)
- [Enlaces](#enlaces)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Introducción

**Cyril Northcote Parkinson** (30 de julio de 1909-9 de marzo de 1993), historiador naval británico, es conocido por su sátira de las instituciones burocráticas [*La ley de Parkinson y otros estudios*](https://es.wikipedia.org/wiki/Ley_de_Parkinson) (Parkinson's Law and other studies), una colección de estudios cortos sobre la inevitabilidad de la expansión burocrática.

![](/doc/chapter-productividad/04-parkinson/images/01.png)

# Ley de Parkinson

Enunciada por el historiador británico [Cyril Northcote Parkinson](http://es.wikipedia.org/wiki/Cyril_Northcote_Parkinson) en 1957:

> “El trabajo se expande hasta que llena todo el tiempo disponible para su realización”

Parkinson se dió cuenta de que, a pesar de haber cada vez menos trabajo burocrático en la Oficina Colonial británica, el número de funcionarios aumentaba cada año en más de un 5% (a pesar de que en ese momento el imperio britanico estaba en retroceso). 

Segun el esto es motivado por dos factores:

1. 'un funcionario quiere multiplicar sus subordinados, no rivales', y
2. 'los funcionarios se crean trabajo unos a otros.'

El resultado de su estudio se publicó en su libro Parkinson’s Law (1957), del que se extrae la conocida [Ley de Parkinson](http://es.wikipedia.org/wiki/Ley_de_Parkinson) (ley de multiplicación de subordinados).

Las tres leyes fundamentales de Parkinson son:

1. "El trabajo se expande hasta llenar el tiempo de que se dispone para su realización".
2. "Los gastos aumentan hasta cubrir todos los ingresos".
3. "El tiempo dedicado a cualquier tema de la agenda es inversamente proporcional a su importancia":  tendencia a dedicar demasiado tiempo a las minuciosidades.

Estas tres leyes, al igual que otras que Parkinson formuló, como la *"ley de la dilación o el arte de perder el tiempo"* y la *"ley de la ocupación de los espacios vacíos"*: por mucho espacio que haya en una oficina siempre hará falta más, son leyes extraídas de la experiencia cotidiana, mediante las cuales, al tiempo que se describe o pone de manifiesto una determinada realidad, se denuncia la falta de eficiencia del trabajo administrativo.

# ¿Como solucionarlo?

Se ve muy fácil cuando se encomienda una tarea a cualquiera, si tenemos un mes de plazo, el trabajo se hará en un mes, aunque pueda hacerse en dos semanas.

* Para muchos, cuanto más tiempo se tenga para hacer algo, más divagará la mente y más problemas serán planteados. En el momento de planificar tu trabajo, márcate unos **plazos mucho más ajustados**. 

* Sé **optimista** en la estimación y acertarás. Limitar el tiempo hace que te obligues a centrarte en lo importante, a ir al grano.

* Cuando hay **recursos limitados encontramos creatividad**, energía, recursos y foco.

* Ley de Parkinson se conecta con nuestra tendencia a complejizar. **Reducir la complejidad** de los procesos internos y la burocracia entre diferentes áreas o equipos de trabajo, visibilizar el trabajo, la automatización de procesos rutinarios pueden ser valsamos para los sistemas complejos o de gran tamaño.

# Conclusiones

La ley de **Parkinson** advierte de dos errores:

* Crecer por un motivo equivocado
* Crear una burocracia interna que se lleve buena parte del tiempo.
* Se debe determinar el alcance de un proyecto o trabajo. Siempre existe una posibilidad de mejora que provocará que el proyecto siga expandiendose en el tiempo. Más allá de cierto tiempo, no hay relación directa con los resultados. 

**La Ley de Parkinson se conecta con la de Pareto**, porque nos obliga a identificar cuál es el 20% de la tarea que es más importante.

# Enlaces

* [**"La ley de Parkinson y otros estudios (Parkinson's Law and other studies)**"](https://es.wikipedia.org/wiki/Ley_de_Parkinson), una colección de estudios cortos sobre la inevitabilidad de la expansión burocrática.  
* ["Las 3 Leyes de la Productividad"](https://facilethings.com/blog/es/laws).
* Wikipedia [Ley de Parkinson](https://es.wikipedia.org/wiki/Ley_de_Parkinson).
* mdzol.com ["¿Qué es la Ley de Productividad de Parkinson?"](https://www.mdzol.com/nota/730250-que-es-la-ley-de-productividad-de-parkinson/).
* ["Pareto Y Parkinson: Dos Principios Para Aumentar Tu Productividad"](http://www.mujeresdeempresa.com/pareto-y-parkinson-dos-principios-para-aumentar-tu-productividad/).
* ["Utiliza la Ley de Parkinson para ser más productivo"](http://jeronimosanchez.com/utiliza-la-ley-de-parkinson-para-ser-mas-productivo/).
* canasto.es ["Cómo ganar 2 horas productivas al día en un instante"](https://canasto.es/blog/2010/03/como-ganar-2-horas-productivas-al-dia-en-un-instante).
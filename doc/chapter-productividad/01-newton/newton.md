<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Primera Ley de Newton sobre el Movimiento ¿Qué tiene que ver la física con la productividad?](#primera-ley-de-newton-sobre-el-movimiento-%C2%BFqu%C3%A9-tiene-que-ver-la-f%C3%ADsica-con-la-productividad)
  - [Las leyes de Newton aplicadas a la productividad](#las-leyes-de-newton-aplicadas-a-la-productividad)
    - [Ley de la inercia](#ley-de-la-inercia)
    - [Ley de la fuerza](#ley-de-la-fuerza)
    - [Principio de acción y reacción](#principio-de-acci%C3%B3n-y-reacci%C3%B3n)
- [Enlaces externos](#enlaces-externos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Primera Ley de Newton sobre el Movimiento ¿Qué tiene que ver la física con la productividad?

![](/doc/chapter-productividad/01-newton/images/01.jpg)

[**Isaac Newton**](https://es.wikipedia.org/wiki/Isaac_Newton) (1642 - 1727) físico y matemático ingles entre otras diversas ocupaciones. Es considerado uno de los científicos más grandes de todos los tiempos. 

Aunque actualmente este episodio se pone en duda es bien conocida la historia de como se inspiro para formular luego la **ley de la Gravedad**. Al parecer, según cuenta William Stukeley, su biógrafo, el científico se encontraba «con ánimo contemplativo» bajo la sombra de un manzano en su granja cuando un fruto cayó del árbol. Entonces se le ocurrió la idea de la gravitación. ¿Por qué la manzana desciende siempre perpecularmente respecto del suelo?, pensó.

En 1687 publica [*"Los principios matemáticos de la filosofía natural"*](https://es.wikipedia.org/wiki/Philosophi%C3%A6_naturalis_principia_mathematica) donde describe sus tres leyes del movimiento. En el proceso **Newton** sentó las bases de la mecánica clásica y cambio la forma en la que el mundo veia la física y la ciencia. 

![](/doc/chapter-productividad/01-newton/images/02.jpg)

**Las tres leyes**:
  
1. Inercia
2. Fuerza
3. Principio de acción-reacción

## Las leyes de Newton aplicadas a la productividad

¿Qué puede aportar un científico nacido en el siglo XVII a las teorías modernas de productividad?

Un señor llamado **James Clear**([@james_clear](https://twitter.com/james_clear)) parece que ha sabido encontrar unos paralelismos entre las tres leyes de Newton y nuestra actitud ante el trabajo. El artículo original se puede consultar en este enlace ["The Physics of Productivity: Newton’s Laws of Getting Stuff Done"](https://jamesclear.com/physics-productivity)  


### Ley de la inercia

> “Todo cuerpo persevera en su estado de reposo o movimiento uniforme y rectilíneo a no ser que sea obligado a cambiar su estado por fuerzas impresas sobre él”

O lo que es lo mismo: **Lo que está en reposo, sigue en reposo; lo que está en movimiento, sigue en movimiento.**

![](/doc/chapter-productividad/01-newton/images/03.png)

Cuando estás perdiendo el tiempo estás en estado de reposo, y como es una situación bastante placentera, es díficil de cambiar. Pero ocurre también que cuando te pones a hacer cosas, y entras en estado de movimiento, es igualmente difícil de parar porque, al fin y al cabo, el hecho de ir completando tareas también resulta satisfactorio.

Ten esto en cuenta, y ponte cada día manos a la obra cuanto antes. Aprende a dar el primer paso haciendo lo que sea. Simplemente, empieza aunque sea con algo pequeño y sin importancia aparente.

Una técnica muy útil para empezar puede ser la llamada "Regla de los Dos Minutos"([How to Stop Procrastinating by Using the “2-Minute Rule”](https://jamesclear.com/how-to-stop-procrastinating)) mostrada en el blog de James Clear:

* Si se toma menos de dos minutos hacerlo comienza ahora, es increible el gran número de cosas que podemos hacer en menos de dos minutos. 
* No todos los objetivos se pueden lograr en menos de dos minutos evidentemente. Empieza con una tarea de menos de dos minutos que te ayude a alcanzar tu objetivo. 

### Ley de la fuerza

> "El cambio de movimiento es proporcional a la fuerza motriz impresa y ocurre según la línea recta a lo largo de la cual aquella fuerza se imprime."

El cambio de movimiento ocurre dependiendo de cuanta fuerza se le aplica a un objeto y en la dirección que se le aplica esta fuerza. Apliquemos esto a la productividad: La fuerza (F) es un vector. Los vectores tienen magnitud (Cuanto esfuerzo estas poniendo) y dirección (Donde se esta enfocando este esfuerzo). 

En otras palabras, si quieres que algo se mueva en una dirección en particular, la cantidad de esfuerzo que pones y en que lo enfocas hace la diferencia. Lo que quiere decir que, **si quieres ser productivo, no solamente depende de que tan duro trabajes (Magnitud), también depende de donde estas aplicando ese trabajo (Dirección).**

Entonces traducido a terminos productivos la clave del éxito de un proyecto depende de:

* la fuerza que apliquemos (impulso o entusiasmo y dedicación) 
* y también de una segunda variable que no debemos olvidar: el foco, o ser capaces de dirigir el proyecto en la dirección adecuada.

### Principio de acción y reacción

> "Con toda acción ocurre siempre una reacción igual y contraria: quiere decir que las acciones mutuas de dos cuerpos siempre son iguales y dirigidas en sentido opuesto."

Y también en el trabajo nos sucede lo mismo: el impulso productivo se ve inevitablemente frenado por fuerzas que van siempre con nosotros, como el estrés, la presión, las interrupciones... En esta situación, tenemos dos opciones: o forzar al máximo la maquinaria para derribar las resistencias, o una más interesante, centrarse en las últimas y lograr reducirlas. La idea básica es evitar todos los obstáculos en el camino que te impidan llevar a efecto tu proyecto.

# Enlaces externos

* hipertextual.com [Newton tenía razón: su lógica también se aplica a la productividad](https://hipertextual.com/2014/11/newton-productividad).
* thenextweb.com [The physics of productivity: Newton’s laws for getting stuff done](https://thenextweb.com/lifehacks/2014/11/23/physics-productivity-newtons-laws-getting-stuff-done/) por James Clear.
* es.insider.pro [Cómo las leyes de Newton pueden ayudarte a ser más eficiente](https://es.insider.pro/lifestyle/2017-06-25/la-fisica-de-la-productividad-como-las-leyes-de-newton-pueden-ayudarte-ser-mas-eficiente/).
* zonageek.net [Las leyes de Newton aplicadas a la productividad](https://www.zonageek.net/las-leyes-de-newton-aplicadas-a-la-productividad/).
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [El principio o ley de Pareto](#el-principio-o-ley-de-pareto)
- [Ley de Pareto aplicada a la productividad](#ley-de-pareto-aplicada-a-la-productividad)
- [Aplicación práctica](#aplicaci%C3%B3n-pr%C3%A1ctica)
  - [TO-DO List](#to-do-list)
- [Prácticas y consejos](#pr%C3%A1cticas-y-consejos)
- [Conclusiones](#conclusiones)
- [Tablero Trello para tareas semanales](#tablero-trello-para-tareas-semanales)
- [Enlaces externos](#enlaces-externos)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# El principio o ley de Pareto

**Vilfredo Pareto** nacido en 1848 de origen Italiano dedicado a diversos campos como la ingeniería, economía o filosofía entre otros. Pareto fue nombrado profesor de economía (1893) en la Universidad de Lausana, donde permaneció el resto de su vida.

En 1906 llego a la conclusión que **el 80% de tierra disponible en Italia estaba en manos del 20% de la población (también regla del 80/20).**

Lo curioso es que este tipo de distribución estadística se puede encontrar en casi en todas partes, no sólo en la economía. 

![](/doc/chapter-productividad/02-pareto/images/01.png)

# Ley de Pareto aplicada a la productividad

**El 80% del éxito de una persona proviene del 20% de su esfuerzo.**

> *"La importancia de la Ley de Pareto para aumentar la productividad radica en entender cómo el 20% de nuestras actividades ocupan el 80% de nuestro tiempo, o cómo el 20% de nuestros esfuerzos nos devuelven un 80% de nuestros resultados."* 

# Aplicación práctica

## TO-DO List

Por ejemplo en tu lista de tareas (TO-DO List) tienes 10 tareas que tienes que completar diariamente. Siguiendo el principio de Pareto debes identificar cuales son las 2 tareas más importantes que debes completar. Querrás invertir en ellas el 80% de tu esfuerzo y el resto en las restantes 8 actividades.

Todas las tareas deben de tener una misma forma de valoración de su relevancia, podemos numerar del 1 al 3 por ejemplo (aplicado a economia clásica puede ser el valor cuantificable en ingresos que genera un cliente o una actividad)

Sabemos que no todas las tareas tienen una recompensa o un resultado inmediato, por lo que habrá que tener en cuenta también el provecho que se espera conseguir a largo plazo.

Significa que aunque no te de tiempo a completar en el día todas las tareas planificadas al menos habrás completado las más importantes.

Dicho de otra forma, la mayor parte del día estamos ocupados haciendo tareas intrascendentes en atención a la poca incidencia que estas tienen en los resultados deseados.

# Prácticas y consejos

* Muchas veces no nos damos cuenta como todos esos minutos que perdemos, al final del día suman horas. Una de las tareas que más tiempo insumen en la actualidad es la gestión del correo electrónico. Si queremos ver una parodía al respecto podemos ver el video [Email in Real Life](https://www.youtube.com/watch?v=HTgYHHKs0Zw). Herramientas conversacionales como Trello o sistemas de mensajería pueden acercarse más a un dialogo o trabajo productivo para generar resultados como veremos más adelante.
* Dedicar tus mejores horarios (¿la mañana?) a aquellas actividades que producen el 80% de tus resultados.
* Priorizar tus tareas y decidir por cuál continuarás.

# Conclusiones

* No se debe tomar como normal general ni en todos los casos, ser crítico ante estas cuestiones.
* Permite simplificar tu vida para enfocarte solo en aquello que es importante.

# Tablero Trello para tareas semanales

Esta es una implementación propia para listar las tareas de cada día de la semana, es muy sencillo y se puede reutilizar una y otra vez. Vale tanto para uso personal como para equipos, cuando lo usamos para equipos facilita saber quien esta trabajando en que.

[TallerTrello - Ejemplo tareas semanales](https://trello.com/b/GtEwBI1l)
    
# Enlaces externos

* ["Pareto Y Parkinson: Dos Principios Para Aumentar Tu Productividad"](http://www.mujeresdeempresa.com/pareto-y-parkinson-dos-principios-para-aumentar-tu-productividad/).
* canasto.es ["Cómo ganar 2 horas productivas al día en un instante"](https://canasto.es/blog/2010/03/como-ganar-2-horas-productivas-al-dia-en-un-instante).
* [The 80/20 Rule: Find And Remove Your Internal Blocks To Improve Productivity](https://blog.trello.com/remove-internal-blocks-improve-productivity).
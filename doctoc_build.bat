@echo off
call doctoc README.md --maxlevel 2
call doctoc doc\chapter-intro-curso\intro-curso.md --maxlevel 2
call doctoc doc\chapter-productividad\01-newton\newton.md --maxlevel 2
call doctoc doc\chapter-productividad\02-pareto\pareto.md --maxlevel 2
call doctoc doc\chapter-productividad\03-eisenhower\eisenhower.md --maxlevel 2


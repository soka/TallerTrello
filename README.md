# Gestión de proyectos con tecnologías colaborativas

**Taller para aprender a trabajar en equipo y aumentar tu productividad con herramientas como [Trello](https://trello.com/) aplicando metodologías ágiles**

Esta es la página principal del taller, en el menú de la izquierda puedes encontrar toda la información necesaria para saciar tu curiosidad e [inscribirte](#inscribete) si te resulta interesante. 

Esta Web principalmente es informátiva, **los contenidos del curso se están mejorando y modificando de forma continua** y es más facil mantenerlos en un tablero de [Trello](https://trello.com). En su debido momento cuando el material este un poco más avanzado el tablero se hará público. 

También recomiendo antes de empezar consultar el apartado de [contacto](#contacto) donde se facilitan recursos para estar en comunicación directa antes del propio curso y esperemos que después si alguien quiere darle continuidad. Aprovecho para invitar a cualquier persona a colaborar desarrollando el propio curso, aprender consiste en construir conocimiento haciendo cosas con otros, a partir de la experiencia y la exploración, del ensayo y error, del análisis y la ejecución. Espero que os guste. Iker.

**Contenidos en esta sección:**

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Información sobre el taller](#informaci%C3%B3n-sobre-el-taller)
- [Inscribete](#inscribete)
- [A quién va dirigido](#a-qui%C3%A9n-va-dirigido)
- [Objetivos](#objetivos)
- [Contacto](#contacto)
  - [El correo electrónico de toda la vida](#el-correo-electr%C3%B3nico-de-toda-la-vida)
  - [Twitter](#twitter)
  - [Slack](#slack)
  - [Telegram](#telegram)
  - [Facebook](#facebook)
- [Resumen contenidos](#resumen-contenidos)
- [Cartel](#cartel)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Información sobre el taller

* El taller es **gratuito**.
* **Fechas impartición**: 27 Miércoles y 28 Jueves de Junio de 19:00 a 21:00 horas.
* **Lugar**: Kultur Etxea de Bilbao La Vieja. C/ Urazurrutia Nº 7 Bilbao ([enlace mapa](https://goo.gl/maps/4zuPh3Egc6m)))

# Inscribete 

**Es indispensable inscribirse en el formulario Web del siguiente [enlace](https://goo.gl/forms/rJfARqgSEwp3v4cA3)**. Llevar un ordenador portátil cobra bastante sentido especialmente para el segundo día donde se abordarán ejercicios prácticos con [Trello](https://trello.com) de forma colaborativa entre todas las personas asistentes. 

# A quién va dirigido

Creo que especialmente se pueden aprovechar de las prácticas y herramientas colaborativas equipos de trabajo, pequeños y medianos colectivos y asociaciones. Más cuando cada vez es más habitual que la gente trabaje de forma remota o en diferentes momentos **venciendo límites temporales  y espaciales**.

A lo largo del taller **también se expondrán algunas técnicas dirigidas a la productividad personal** y se expondrán ejemplos de como aplicarlas mediante herramientas para reducir el estres y la ansiedad que nos provoca la sensación de no poder abarcar todo lo que queriamos hacer y los días siempre se nos quedan cortos. 

En los **equipos remotos** se trata de favorecer la colaboración a distancia a través de la tecnología. La **comunicación fluida y la transparencia** (los pilares de algunas metodologías que se exponen en el taller) son fundamentales.

# Objetivos

Al finalizar el curso se busca que adquieras o mejores habilidades para:

* Potenciar equipos de trabajo y mejorar su comunicación.
* Familiarizarse con nuevas metodologías de trabajo ágiles y algunas técnicas de productividad personal.    
* Conocer y saber aplicar las prácticas fundamentales que se proponen aquí de forma general.
* Conocer [**Trello**](https://trello.com) y aplicar las metodologías arriba mencionadas con ejemplos practicos. 

# Contacto

## El correo electrónico de toda la vida 

El medio más habitual es mediante **Correo electrónico**, puedes escribir a cualquiera de las siguientes direcciones: ikastaroak.kulturetxea@gmail.com, kulturetxeabizirik@gmail.com o ikernaix@gmail.com.

## Twitter

Para seguirme o seguirnos en **Twitter**: [@KulturEtxea](https://twitter.com/KulturEtxea) o [@ikernaix](https://twitter.com/ikernaix).

## Slack

[**Slack**](https://slack.com/) Es una aplicación de mensajería, el espacio de trabajo se ordena en canales. Slack puede combinar a la perfección con [**Trello**](https://trello.com), tiene aplicación nativa para smartphones ([Slack para Android en Google Play](https://play.google.com/store/apps/details?id=com.Slack)). 

## Telegram

Habilitado canal público para el sistema de mensajería móvil y Web de [Telegram](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=es), una vez instalado Telegram usar el siguiente enlace ["TallerTrello"](https://t.me/TallerTrello). Los canales en Telegram solo permiten a difusión unidireccional del creador del canal, los usuarios no pueden escribir, lo usaremos para propósitos informátivos únicamente.

<!-- ![](/doc/chapter-intro-curso/images/02.png) -->

## Facebook

[Enlace al evento en Facebook](https://www.facebook.com/events/1841737346131510/).

# Resumen contenidos

**Día 1**:

* Introducción al taller.
* Productividad personal.
* Gestión de proyectos y metodologías ágiles.

**Día 2**:

* Trello.

# Cartel

Este es el cartel en tamaño original ideal para su difusión, la resolución está bastante ajustada para que pese poco y tiene un tamaño de 842 x 595 px.

![](/doc/chapter-intro-curso/images/cartel.png)

Banner:

![](/doc/chapter-intro-curso/images/banner.png)
